﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sensum_Spa.Modulos.Efectivo
{
    /// <summary>
    /// Lógica de interacción para RetiroEfectivo.xaml
    /// </summary>
    public partial class RetiroEfectivo : Window
    {
        BibliotecaClases.Conexion cone = new BibliotecaClases.Conexion();
        List<string> listaClientes = new List<string>();
        public delegate void DataChangedEventHandler(object sender, EventArgs e);
        public event DataChangedEventHandler DataChanged;
        public RetiroEfectivo()
        {
            InitializeComponent();
        }
        
        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void txtCantidad_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            // Use SelectionStart property to find the caret position.
            // Insert the previewed text into the existing text in the textbox.
            var fullText = textBox.Text.Insert(textBox.SelectionStart, e.Text);

            decimal val;
            // If parsing is successful, set Handled to false
            e.Handled = !decimal.TryParse(fullText, out val);
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (txtConcepto.Text == "" || txtCantidad.Text == "")
            {
                MessageBox.Show("No puede dejar campos vacios", "INFORMATIVO", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                if (MessageBox.Show("¿Estas seguro(a) de realizar el registro?", "PREGUNTA", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        cone.RetiroEfectivo(txtTipoPago.Text, txtConcepto.Text, Convert.ToDecimal(txtCantidad.Text));
                        MessageBox.Show("¡El registro se realizo correctamente!", "EXITO", MessageBoxButton.OK, MessageBoxImage.Information);

                        DataChangedEventHandler handler = DataChanged;

                        if (handler != null)
                        {
                            handler(this, new EventArgs());
                        }

                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }
    }
}
