﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sensum_Spa.Modulos.Efectivo
{
    /// <summary>
    /// Lógica de interacción para IngresoEfectivo.xaml
    /// </summary>
    public partial class IngresoEfectivo : Window
    {
        #region Variables globales
        BibliotecaClases.Conexion cone = new BibliotecaClases.Conexion();
        List<BibliotecaClases.TipoPago> listaTipoPago = new List<BibliotecaClases.TipoPago>();
        List<string> listaClientes = new List<string>();
        List<string> listaServicios = new List<string>();
        public delegate void DataChangedEventHandler(object sender, EventArgs e);
        public event DataChangedEventHandler DataChanged;
        #endregion

        #region Constructor
        public IngresoEfectivo()
        {
            InitializeComponent();
            try
            {
                comboboxTipoPago();
                listaClientes = cone.obtenerSoloClientes();
                listaServicios = cone.obtenerServiciosString();
                txtCliente.TextChanged += new TextChangedEventHandler(txtCliente_TextChanged);
                txtDescripcion.TextChanged += new TextChangedEventHandler(txtDescripcion_TextChanged);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region Botones
        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (txtCliente.Text == "" || txtDescripcion.Text == "" || txtCantidad.Text == "")
            {
                MessageBox.Show("No puede dejar campos vacios.", "INFORMATIVO", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (txtIDCliente.Text == "")
            {
                MessageBox.Show("El cliente no cuenta con ID.", "INFORMATIVO", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                if (MessageBox.Show("¿Estas seguro(a) de realizar el registro?", "PREGUNTA", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        if (txtPrecio.Text == "")
                            txtPrecio.Text = "0";

                        cone.ingresoEfectivo(cmbTipoPago.Text, Convert.ToInt32(txtIDCliente.Text), txtDescripcion.Text, Convert.ToDecimal(txtCantidad.Text), Convert.ToDecimal(txtPrecio.Text));
                        MessageBox.Show("¡El registro se realizo correctamente!", "EXITO", MessageBoxButton.OK, MessageBoxImage.Information);

                        DataChangedEventHandler handler = DataChanged;

                        if (handler != null)
                        {
                            handler(this, new EventArgs());
                        }

                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }
        #endregion

        #region Metodos
        private void comboboxTipoPago()
        {
            listaTipoPago = cone.obtenerInfoTipoPago();

            cmbTipoPago.ItemsSource = listaTipoPago;
            cmbTipoPago.DisplayMemberPath = "Descripcion";
            cmbTipoPago.SelectedValuePath = "IDTipoPago";

            cmbTipoPago.SelectedValue = "1";
        }
        private string devuelveCliente(string str)
        {
            int index = str.IndexOf('-');
            string IDCliente = "";
            if (index > 0)
            {
                IDCliente = str.Substring(0, index);
            }
            return IDCliente;
        }
        private string devuelveServicio(string str)
        {
            int index = str.IndexOf('-');
            string IDServicio = "";
            if (index > 0)
            {
                IDServicio = str.Substring(0, index);
            }
            return IDServicio;
        }
        #endregion

        #region Eventos
        private void txtCliente_TextChanged(object sender, TextChangedEventArgs e)
        {
            string typedString = txtCliente.Text;
            List<string> autoList = new List<string>();
            
            autoList.Clear();
            foreach (string item in listaClientes)
            {
                if (!string.IsNullOrEmpty(txtCliente.Text))
                {
                    if (item.StartsWith(typedString))
                    {
                        autoList.Add(item);
                    }
                }
            }
            if (autoList.Count > 0)
            {
                lbSuggestion.ItemsSource = autoList;
                lbSuggestion.Visibility = Visibility.Visible;
            }
            else if (txtCliente.Text.Equals(""))
            {
                txtIDCliente.Text = "";
                lbSuggestion.Visibility = Visibility.Collapsed;
                lbSuggestion.ItemsSource = null;
            }
            else
            {
                lbSuggestion.Visibility = Visibility.Collapsed;
                lbSuggestion.ItemsSource = null;
            }
        }
        private void lbSuggestion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestion.ItemsSource != null)
            {
                try
                {
                    string Cliente = "";
                    string IDCliente = "";
                    lbSuggestion.Visibility = Visibility.Collapsed;
                    txtCliente.TextChanged -= new TextChangedEventHandler(txtCliente_TextChanged);

                    if (lbSuggestion.SelectedIndex != -1)
                    {
                        Cliente = devuelveCliente(lbSuggestion.SelectedItem.ToString());
                        txtCliente.Text = Cliente;

                        IDCliente = lbSuggestion.SelectedItem.ToString().Split('-')[1];
                        IDCliente = IDCliente.Substring(0, 1).ToUpper() + IDCliente.Substring(1);
                        txtIDCliente.Text = IDCliente;
                        lbSuggestion.Visibility = Visibility.Collapsed;
                    }
                    txtCliente.TextChanged += new TextChangedEventHandler(txtCliente_TextChanged);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void txtCantidad_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            // Use SelectionStart property to find the caret position.
            // Insert the previewed text into the existing text in the textbox.
            var fullText = textBox.Text.Insert(textBox.SelectionStart, e.Text);

            decimal val;
            // If parsing is successful, set Handled to false
            e.Handled = !decimal.TryParse(fullText, out val);
        }

        private void txtDescripcion_TextChanged(object sender, TextChangedEventArgs e)
        {
            string typedString = txtDescripcion.Text;
            List<string> autoList = new List<string>();

            autoList.Clear();
            foreach (string item in listaServicios)
            {
                if (!string.IsNullOrEmpty(txtDescripcion.Text))
                {
                    if (item.StartsWith(typedString))
                    {
                        autoList.Add(item);
                    }
                }
            }
            if (autoList.Count > 0)
            {
                lbSuggestionServ.ItemsSource = autoList;
                lbSuggestionServ.Visibility = Visibility.Visible;
            }
            else if (txtDescripcion.Text.Equals(""))
            {
                txtPrecio.Text = "";
                lbSuggestionServ.Visibility = Visibility.Collapsed;
                lbSuggestionServ.ItemsSource = null;
            }
            else
            {
                lbSuggestionServ.Visibility = Visibility.Collapsed;
                lbSuggestionServ.ItemsSource = null;
            }
        }

        private void lbSuggestionServ_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestionServ.ItemsSource != null)
            {
                try
                {
                    string Servicio = "";
                    string Precio = "";

                    lbSuggestionServ.Visibility = Visibility.Collapsed;
                    txtDescripcion.TextChanged -= new TextChangedEventHandler(txtDescripcion_TextChanged);

                    if (lbSuggestionServ.SelectedIndex != -1)
                    {
                        Servicio = devuelveServicio(lbSuggestionServ.SelectedItem.ToString());
                        txtDescripcion.Text = Servicio;

                        Precio = lbSuggestionServ.SelectedItem.ToString().Split('-')[1];
                        Precio = Precio.Substring(0, 1).ToUpper() + Precio.Substring(1);
                        txtCantidad.Text = Precio;
                        txtPrecio.Text = Precio;
                        lbSuggestionServ.Visibility = Visibility.Collapsed;
                    }
                    txtDescripcion.TextChanged += new TextChangedEventHandler(txtDescripcion_TextChanged);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        #endregion
    }
}
    

