﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sensum_Spa.Modulos.Efectivo
{
    /// <summary>
    /// Lógica de interacción para TicketPago.xaml
    /// </summary>
    public partial class TicketPago : Window
    {
        public int IDCaja = 0;
        BibliotecaClases.ServicioCliente objServicioClientes = new BibliotecaClases.ServicioCliente();
        BibliotecaClases.Conexion cone = new BibliotecaClases.Conexion();
        public TicketPago(int idCaja)
        {
            InitializeComponent();
            IDCaja = idCaja;

            try
            {
                objServicioClientes = cone.obtenerInfoTicket(IDCaja);

                txbFolio.Text = objServicioClientes.Folio;
                txbServicio.Text = objServicioClientes.Servicio;
                string precio = Convert.ToString(objServicioClientes.Precio);
                txbPrecio.Text = precio;
                string importe = Convert.ToString(objServicioClientes.Importe);
                txbImporte.Text = importe;
                string saldoPendiente = Convert.ToString(objServicioClientes.SaldoPendiente);
                txbSaldoPendiente.Text = saldoPendiente;
                txbIDPagoServicio.Text = objServicioClientes.IDPagoServicioCliente;
                txbCliente.Text = objServicioClientes.NombreCliente;
                txbFecha.Text = objServicioClientes.FechaCompra;
                txbClienteFirma.Text = objServicioClientes.NombreCliente;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
