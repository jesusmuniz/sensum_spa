﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sensum_Spa.Modulos.Servicios
{
    /// <summary>
    /// Lógica de interacción para ServiciosPrincipal.xaml
    /// </summary>
    public partial class ServiciosPrincipal : Window
    {
        #region Variables globales
        BibliotecaClases.Conexion cone = new BibliotecaClases.Conexion();
        List<BibliotecaClases.Servicio> listaServicios = new List<BibliotecaClases.Servicio>();
        DataTable _dataTable;
        #endregion

        #region Constructor
        public ServiciosPrincipal()
        {
            InitializeComponent();
            try
            {
                listaServicios = cone.obtenerInfoServicios();
                convertToDatatable(listaServicios);
                dataGridServicios.CellEditEnding += dataGridServicios_CellEditEnding;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region Metodos
        private DataTable convertToDatatable(List<BibliotecaClases.Servicio> list)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add("IDServicio");
            _dataTable.Columns.Add("TipoServicio");
            _dataTable.Columns.Add("Servicio");
            _dataTable.Columns.Add("Precio");
            _dataTable.Columns.Add("Fecha");

            foreach (var item in listaServicios)
            {
                var row = _dataTable.NewRow();

                row["IDServicio"] = item.IDServicio;
                row["TipoServicio"] = item.TipoServicio;
                row["Servicio"] = item.Descripcion;
                row["Precio"] = item.Precio;
                row["Fecha"] = item.Fecha;

                _dataTable.Rows.Add(row);
                dataGridServicios.ItemsSource = _dataTable.DefaultView;
            }
            return _dataTable;
        }
        private string getSelectedValue(DataGrid grid)
        {
            DataGridCellInfo cellInfo = grid.SelectedCells[0];
            if (cellInfo == null)
                return null;

            DataGridBoundColumn column = cellInfo.Column as DataGridBoundColumn;

            if (column == null)
                return null;

            FrameworkElement element = new FrameworkElement() { DataContext = cellInfo.Item };
            BindingOperations.SetBinding(element, TagProperty, column.Binding);

            return element.Tag.ToString();
        }
        #endregion

        #region Eventos
        private void txtServicios_TextChanged(object sender, TextChangedEventArgs e)
        {
            string filter = txtServicios.Text;

            if (string.IsNullOrEmpty(filter))
                _dataTable.DefaultView.RowFilter = null;
            else
                _dataTable.DefaultView.RowFilter = string.Format("Servicio Like '%{0}%'", filter);
        }
        private void dataGridServicios_CopyingRowClipboardContent(object sender, DataGridRowClipboardEventArgs e)
        {
            var currentCell = e.ClipboardRowContent[dataGridServicios.CurrentCell.Column.DisplayIndex];
            e.ClipboardRowContent.Clear();
            e.ClipboardRowContent.Add(currentCell);
        }
        private void dataGridServicios_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
            {
                var column = e.Column as DataGridBoundColumn;

                if (column != null)
                {
                    var bindingPath = (column.Binding as Binding).Path.Path;

                    if (bindingPath == "Precio")
                    {
                        string IDServicio = "";
                        int rowIndex = e.Row.GetIndex();
                        var precio = e.EditingElement as TextBox;
                        decimal precioNuevo = 0;
                        decimal.TryParse(precio.Text, out precioNuevo);
                        IDServicio = getSelectedValue(dataGridServicios);

                        if (MessageBox.Show("¿Estas seguro(a) de cambiar este precio?", "PREGUNTA", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            try
                            {
                                cone.editarPrecioServicio(Convert.ToInt32(IDServicio), precioNuevo);
                                MessageBox.Show("¡El cambio se realizo correctamente!", "EXITO", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region
        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
