﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sensum_Spa.Modulos.Clientes
{
    /// <summary>
    /// Lógica de interacción para SaldoCliente.xaml
    /// </summary>
    public partial class SaldoCliente : Window
    {
        #region Variables globales
        BibliotecaClases.Conexion cone = new BibliotecaClases.Conexion();
        BibliotecaClases.Clientes objClientes = new BibliotecaClases.Clientes();
        List<BibliotecaClases.ServicioCliente> listaServicioCliente = new List<BibliotecaClases.ServicioCliente>();
        private int IDCliente = 0;
        #endregion

        #region Constructor
        public SaldoCliente(int idCliente)
        {
            InitializeComponent();
            IDCliente = idCliente;

            try
            {
                objClientes = cone.obtenerNombreCliente(IDCliente);
                txbCliente.Text = objClientes.Nombre;
                listaServicioCliente = cone.obtenerInfoServicioCliente(IDCliente);
                dataGridSaldoCliente.ItemsSource = listaServicioCliente;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region
        private void dataGridSaldoCliente_CopyingRowClipboardContent(object sender, DataGridRowClipboardEventArgs e)
        {
            var currentCell = e.ClipboardRowContent[dataGridSaldoCliente.CurrentCell.Column.DisplayIndex];
            e.ClipboardRowContent.Clear();
            e.ClipboardRowContent.Add(currentCell);
        }
        #endregion
    }
}
