﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sensum_Spa.Modulos.Clientes
{
    /// <summary>
    /// Lógica de interacción para ClientesPrincipal2.xaml
    /// </summary>
    public partial class ClientesPrincipal : Window
    {
        #region Variables globales
        BibliotecaClases.Conexion cone = new BibliotecaClases.Conexion();
        List<BibliotecaClases.Clientes> listaClientes = new List<BibliotecaClases.Clientes>();
        DataTable _dataTable;
        #endregion

        #region Constructor
        public ClientesPrincipal()
        {
            InitializeComponent();
            try
            {
                listaClientes = cone.obtenerClientes();
                convertToDatatable(listaClientes);
                dataGridClientes.CellEditEnding += dataGridClientes_CellEditEnding;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region Metodos
        private DataTable convertToDatatable(List<BibliotecaClases.Clientes> list)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add("IDCliente");
            _dataTable.Columns.Add("Nombre");
            _dataTable.Columns.Add("ApellidoPaterno");
            _dataTable.Columns.Add("ApellidoMaterno");
            _dataTable.Columns.Add("Telefono");
            _dataTable.Columns.Add("Celular");
            _dataTable.Columns.Add("CorreoElectronico");

            foreach (var item in listaClientes)
            {
                var row = _dataTable.NewRow();

                row["IDCliente"] = item.IDCliente;
                row["Nombre"] = item.Nombre;
                row["ApellidoPaterno"] = item.ApellidoPaterno;
                row["ApellidoMaterno"] = item.ApellidoMaterno;
                row["Telefono"] = item.Telefono;
                row["Celular"] = item.Celular;
                row["CorreoElectronico"] = item.CorreoElectronico;

                _dataTable.Rows.Add(row);
                dataGridClientes.ItemsSource = _dataTable.DefaultView;
            }
            return _dataTable;
        }
        private string getSelectedValue(DataGrid grid)
        {
            DataGridCellInfo cellInfo = grid.SelectedCells[0];
            if (cellInfo == null)
                return null;

            DataGridBoundColumn column = cellInfo.Column as DataGridBoundColumn;

            if (column == null)
                return null;

            FrameworkElement element = new FrameworkElement() { DataContext = cellInfo.Item };
            BindingOperations.SetBinding(element, TagProperty, column.Binding);

            return element.Tag.ToString();
        }
        #endregion

        #region Botones
        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Eventos
        private void dataGridClientes_CopyingRowClipboardContent(object sender, DataGridRowClipboardEventArgs e)
        {
            var currentCell = e.ClipboardRowContent[dataGridClientes.CurrentCell.Column.DisplayIndex];
            e.ClipboardRowContent.Clear();
            e.ClipboardRowContent.Add(currentCell);
        }

        private void txtClientes_TextChanged(object sender, TextChangedEventArgs e)
        {
            string filter = txtClientes.Text;

            if (string.IsNullOrEmpty(filter))
                _dataTable.DefaultView.RowFilter = null;
            else
                _dataTable.DefaultView.RowFilter = string.Format("Nombre Like '%{0}%' OR ApellidoPaterno Like '%{0}%'", filter);
        }

        private void txtAgregarClientes_Click(object sender, RoutedEventArgs e)
        {
            var window = new Sensum_Spa.Modulos.Clientes.AgregarCliente();
            window.DataChanged += ClientesPrincipal_DataChanged;
            window.Show();
        }
        private void ClientesPrincipal_DataChanged(object sender, EventArgs e)
        {
            listaClientes = cone.obtenerClientes();
            convertToDatatable(listaClientes);
        }

        private void dataGridClientes_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string CellValue = "";
            if (dataGridClientes.SelectedCells.Count > 0)
            {
                CellValue = getSelectedValue(dataGridClientes);
                
                Modulos.Clientes.SaldoCliente myOwnedWindow = new Modulos.Clientes.SaldoCliente(Convert.ToInt32(CellValue));
                myOwnedWindow.Owner = this;
                myOwnedWindow.Show();
            }
        }
        private void dataGridClientes_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
            {
                var column = e.Column as DataGridBoundColumn;

                if (column != null)
                {
                    try
                    {
                        var bindingPath = (column.Binding as Binding).Path.Path;
                        string IDCliente = "";
                        int rowIndex = e.Row.GetIndex();
                        IDCliente = getSelectedValue(dataGridClientes);

                        if (bindingPath == "Nombre")
                        {
                            var nombre = e.EditingElement as TextBox;
                            string nuevoNombre = Convert.ToString(nombre.Text);

                            if (MessageBox.Show("¿Estas seguro(a) de cambiar la informacioón de este cliente?", "PREGUNTA", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                cone.editarInfoCliente(Convert.ToInt32(IDCliente), nuevoNombre, null, null, null, null, null);
                                MessageBox.Show("¡El cambio se realizo correctamente!", "EXITO", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else if (bindingPath == "ApellidoPaterno")
                        {
                            var apellidoPaterno = e.EditingElement as TextBox;
                            string nuevoApellidoPaterno = Convert.ToString(apellidoPaterno.Text); 

                            if (MessageBox.Show("¿Estas seguro(a) de cambiar la informacioón de este cliente?", "PREGUNTA", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                cone.editarInfoCliente(Convert.ToInt32(IDCliente), null, nuevoApellidoPaterno, null, null, null, null);
                                MessageBox.Show("¡El cambio se realizo correctamente!", "EXITO", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else if (bindingPath == "ApellidoMaterno")
                        {
                            var apellidoMaterno = e.EditingElement as TextBox;
                            string nuevoApellidoMaterno = Convert.ToString(apellidoMaterno.Text);

                            if (MessageBox.Show("¿Estas seguro(a) de cambiar la informacioón de este cliente?", "PREGUNTA", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                cone.editarInfoCliente(Convert.ToInt32(IDCliente), null, null, nuevoApellidoMaterno, null, null, null);
                                MessageBox.Show("¡El cambio se realizo correctamente!", "EXITO", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else if (bindingPath == "Telefono")
                        {
                            var telefono = e.EditingElement as TextBox;
                            string nuevoTelefono = Convert.ToString(telefono.Text);

                            if (MessageBox.Show("¿Estas seguro(a) de cambiar la informacioón de este cliente?", "PREGUNTA", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                cone.editarInfoCliente(Convert.ToInt32(IDCliente), null, null, null, nuevoTelefono, null, null);
                                MessageBox.Show("¡El cambio se realizo correctamente!", "EXITO", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else if (bindingPath == "Celular")
                        {
                            var celular = e.EditingElement as TextBox;
                            string nuevoCelular = Convert.ToString(celular.Text);

                            if (MessageBox.Show("¿Estas seguro(a) de cambiar la informacioón de este cliente?", "PREGUNTA", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                cone.editarInfoCliente(Convert.ToInt32(IDCliente), null, null, null, null, nuevoCelular, null);
                                MessageBox.Show("¡El cambio se realizo correctamente!", "EXITO", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else if (bindingPath == "CorreoElectronico")
                        {
                            var correoElectronico = e.EditingElement as TextBox;
                            string nuevoCorreoElectronico = Convert.ToString(correoElectronico.Text);

                            if (MessageBox.Show("¿Estas seguro(a) de cambiar la informacioón de este cliente?", "PREGUNTA", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                cone.editarInfoCliente(Convert.ToInt32(IDCliente), null, null, null, null, null, nuevoCorreoElectronico);
                                MessageBox.Show("¡El cambio se realizo correctamente!", "EXITO", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }
        #endregion
    }
}
