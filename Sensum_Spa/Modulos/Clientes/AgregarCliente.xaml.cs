﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sensum_Spa.Modulos.Clientes
{
    /// <summary>
    /// Lógica de interacción para AgregarCliente.xaml
    /// </summary>
    public partial class AgregarCliente : Window
    {
        BibliotecaClases.Conexion cone = new BibliotecaClases.Conexion();
        public delegate void DataChangedEventHandler(object sender, EventArgs e);
        public event DataChangedEventHandler DataChanged;
        public AgregarCliente()
        {
            InitializeComponent();
        }
        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if(txtNombre.Text == "" || txtApellidoPaterno.Text == "" || txtApellidoMaterno.Text == "" || txtCelular.Text == "")
            {
                MessageBox.Show("Los campos marcados como obligatorios no pueden ir vacios.", "INFORMATIVO", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                if (MessageBox.Show("¿Estas seguro(a) de realizar el registro?", "PREGUNTA", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        cone.guardarCliente(txtNombre.Text, txtApellidoPaterno.Text, txtApellidoMaterno.Text, txtTelefono.Text, txtCelular.Text, txtCorreo.Text);
                        MessageBox.Show("¡El registro se realizo correctamente!", "EXITO", MessageBoxButton.OK, MessageBoxImage.Information);

                        DataChangedEventHandler handler = DataChanged;

                        if (handler != null)
                        {
                            handler(this, new EventArgs());
                        }

                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }
    }
}
