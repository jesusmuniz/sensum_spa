﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sensum_Spa
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Variables globales
        BibliotecaClases.Conexion cone = new BibliotecaClases.Conexion();
        List<BibliotecaClases.Caja> listaCaja = new List<BibliotecaClases.Caja>();
        #endregion

        #region Constructor
        public MainWindow()
        {
            InitializeComponent();

            try 
            {
                listaCaja = cone.obtenerInfoCaja();
                dataGridCaja.ItemsSource = listaCaja;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region Botones
        private void btnIngEfectivo_Click(object sender, RoutedEventArgs e)
        {
            var window = new Sensum_Spa.Modulos.Efectivo.IngresoEfectivo();
            window.DataChanged += MainWindow_DataChanged;
            window.Show();
        }
        private void btnRetEfectivo_Click(object sender, RoutedEventArgs e)
        {
            var window = new Sensum_Spa.Modulos.Efectivo.RetiroEfectivo();
            window.DataChanged += MainWindow_DataChanged;
            window.Show();
        }
        #endregion

        #region Metodos
        private string getSelectedValue(DataGrid grid)
        {
            DataGridCellInfo cellInfo = grid.SelectedCells[0];
            if (cellInfo == null)
                return null;

            DataGridBoundColumn column = cellInfo.Column as DataGridBoundColumn;

            if (column == null)
                return null;

            FrameworkElement element = new FrameworkElement() { DataContext = cellInfo.Item };
            BindingOperations.SetBinding(element, TagProperty, column.Binding);

            return element.Tag.ToString();
        }
        #endregion

        #region Eventos
        private void MenuItem_Click_Clientes(object sender, RoutedEventArgs e)
        {
            Modulos.Clientes.ClientesPrincipal myOwnedWindow = new Modulos.Clientes.ClientesPrincipal();
            myOwnedWindow.Owner = this;
            myOwnedWindow.Show();
        }

        private void MainWindow_DataChanged(object sender, EventArgs e)
        {
            listaCaja = cone.obtenerInfoCaja();
            dataGridCaja.ItemsSource = listaCaja;
        }

        private void dataGridCaja_CopyingRowClipboardContent(object sender, DataGridRowClipboardEventArgs e)
        {
            var currentCell = e.ClipboardRowContent[dataGridCaja.CurrentCell.Column.DisplayIndex];
            e.ClipboardRowContent.Clear();
            e.ClipboardRowContent.Add(currentCell);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Modulos.Servicios.ServiciosPrincipal myOwnedWindow = new Modulos.Servicios.ServiciosPrincipal();
            myOwnedWindow.Owner = this;
            myOwnedWindow.Show();
        }

        private void dataGridCaja_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string CellValue = "";

            if (dataGridCaja.SelectedCells.Count > 0)
            {
                CellValue = getSelectedValue(dataGridCaja);

                Modulos.Efectivo.TicketPago myOwnedWindow = new Modulos.Efectivo.TicketPago(Convert.ToInt32(CellValue));

                PrintDialog printDialog = new PrintDialog();

                printDialog.PrintVisual(myOwnedWindow.gridTicket, "Ticket de Pago");
                myOwnedWindow.Close();
            }
        }
        #endregion
    }
}
