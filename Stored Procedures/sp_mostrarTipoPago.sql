USE [Sensum]
GO
/****** Object:  StoredProcedure [dbo].[sp_mostrarTipoPago]    Script Date: 19/11/2021 11:49:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 10 de octubre de 2021
-- Description:	Muestra los tipos de pago existentes
-- =============================================
ALTER PROCEDURE [dbo].[sp_mostrarTipoPago] 
AS
BEGIN
	SELECT
		IDTipoPago,
		Descripcion
	FROM 
		TipoPago
END
