USE [Sensum]
GO
/****** Object:  StoredProcedure [dbo].[sp_obtenerNombreCliente]    Script Date: 19/11/2021 11:49:58 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 19 de noviembre de 2021
-- Description:	Obtiene el nombre completo de un cliente mediante su ID.
-- =============================================
ALTER PROCEDURE [dbo].[sp_obtenerNombreCliente] 
	@IDCliente AS INTEGER
AS
BEGIN
	SELECT 
		Nombre + ' ' + ApellidoPaterno + ' ' + ApellidoMaterno AS NombreCliente
	FROM
		Cliente
	WHERE
		IDCliente = @idCliente
END
