USE [Sensum]
GO
/****** Object:  StoredProcedure [dbo].[sp_guardarRetiroEfectivo]    Script Date: 19/11/2021 11:46:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz Gonzalezc
-- Description:	Permite guardar los registros de reiro en efectivo
-- =============================================
ALTER PROCEDURE [dbo].[sp_guardarRetiroEfectivo] 
	@tipoPago AS VARCHAR(30),
	@concepto AS VARCHAR(200),
	@cantidad MONEY
AS
BEGIN
	DECLARE @idTipoPago AS INT,
			@saldoCajaActual AS MONEY,
			@msg AS VARCHAR(100),
			@IDIngresoEfectivo AS INT,
			@IDRetiroEfectivo AS INT

	SELECT @idTipoPago = IDTipoPago FROM TipoPago NOLOCK WHERE Descripcion = @tipoPago
	SELECT TOP 1 @saldoCajaActual = Cantidad FROM Caja NOLOCK ORDER BY IDCaja DESC
	SELECT TOP 1 @IDIngresoEfectivo = IDIngresoEfectivo FROM IngresoEfectivo NOLOCK ORDER BY IDIngresoEfectivo DESC

	BEGIN TRANSACTION
		SET @saldoCajaActual = @saldoCajaActual - @cantidad

		INSERT INTO RetiroEfectivo(Fecha, Concepto, Cantidad)
		VALUES(GETDATE(), UPPER(@concepto), @cantidad)

		SET @IDRetiroEfectivo = SCOPE_IDENTITY()

		INSERT INTO Caja(IDIngresoEfectivo, IDRetiroEfectivo, Cantidad)
		VALUES(@IDIngresoEfectivo, @IDRetiroEfectivo, @saldoCajaActual)
	COMMIT TRANSACTION

	SET @msg = 'El registro se realizo correctamente.'
	SELECT @msg AS Mensaje
END
