USE [Sensum]
GO
/****** Object:  StoredProcedure [dbo].[sp_guardarNuevoCliente]    Script Date: 19/11/2021 11:46:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz Gonzalez
-- Create date: 22 de octubre de 2021
-- Description:	Permite guardar un nuevo cliente con un saldo inicial
-- =============================================
ALTER PROCEDURE [dbo].[sp_guardarNuevoCliente]
	@nombre AS VARCHAR(50),
	@apellidoPaterno AS VARCHAR(30),
	@apellidoMaterno AS VARCHAR(30),
	@telefomo AS VARCHAR(10),
	@celular AS VARCHAR(10),
	@correoElectronico AS VARCHAR(50)
AS
	BEGIN
		DECLARE @msg varchar(100),
				@IDCliente AS INT,
				@IDCaja AS MONEY

		IF EXISTS(SELECT 0 FROM Cliente WHERE Nombre = UPPER(@nombre) AND ApellidoPaterno = UPPER(@apellidoPaterno) AND ApellidoMaterno = UPPER(@apellidoMaterno))
			BEGIN
				SET @msg = 'El cliente ya se encuentra registrado.'
				RAISERROR (@msg, 16, 1)
			END
		ELSE
			BEGIN
				BEGIN TRANSACTION
					INSERT INTO Cliente(Nombre, ApellidoPaterno, ApellidoMaterno, Telefono, Celular, CorreoElectronico, Activo)
					VALUES(UPPER(@nombre), UPPER(@apellidoPaterno), UPPER(@apellidoMaterno), @telefomo, @celular, UPPER(@correoElectronico), 1)
				COMMIT TRANSACTION

				SET @msg = 'El registro se realizo correctamente.'
				SELECT @msg AS Mensaje
			END
	END
