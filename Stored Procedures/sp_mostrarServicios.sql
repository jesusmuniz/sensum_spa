USE [Sensum]
GO
/****** Object:  StoredProcedure [dbo].[sp_mostrarServicios]    Script Date: 23/11/2021 07:54:24 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 07 de noviembre de 2021
-- Description:	Permite mostrar la lista de servicios disponibles
-- =============================================
ALTER PROCEDURE [dbo].[sp_mostrarServicios]

AS
BEGIN
	SELECT 
		a.IDServicio AS IDServicio,
		b.Descripcion AS TipoServicio,
		a.Descripcion AS Servicio,
		a.Precio AS Precio,
		a.Fecha AS Fecha
	FROM 
		Servicio AS a
		LEFT OUTER JOIN TipoServicio AS b
		ON a.IDTipoServicio = b.IDTipoServicio
	WHERE
		a.Activo = 1
	ORDER BY b.IDTipoServicio DESC
END
