USE [Sensum]
GO
/****** Object:  Table [dbo].[Caja]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Caja](
	[IDCaja] [int] IDENTITY(1,1) NOT NULL,
	[IDIngresoEfectivo] [int] NULL,
	[IDRetiroEfectivo] [int] NULL,
	[Cantidad] [money] NULL,
 CONSTRAINT [PK_Ingreso] PRIMARY KEY CLUSTERED 
(
	[IDCaja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[IDCliente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[ApellidoPaterno] [varchar](30) NULL,
	[ApellidoMaterno] [varchar](30) NULL,
	[Telefono] [varchar](10) NULL,
	[Celular] [varchar](10) NULL,
	[CorreoElectronico] [varchar](50) NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[IDCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IngresoEfectivo]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IngresoEfectivo](
	[IDIngresoEfectivo] [int] IDENTITY(1,1) NOT NULL,
	[IDTipoPago] [int] NULL,
	[IDCliente] [int] NULL,
	[Fecha] [datetime] NULL,
	[Concepto] [varchar](200) NULL,
	[Cantidad] [money] NULL,
 CONSTRAINT [PK_IngresoEfectivo] PRIMARY KEY CLUSTERED 
(
	[IDIngresoEfectivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PagoServicioCliente]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PagoServicioCliente](
	[IDPagoServicioCliente] [int] IDENTITY(1,1) NOT NULL,
	[IDIngresoEfectivo] [int] NULL,
	[FolioServicio] [varchar](10) NULL,
	[Pendiente] [money] NULL,
 CONSTRAINT [PK_PagoServicioCliente] PRIMARY KEY CLUSTERED 
(
	[IDPagoServicioCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RetiroEfectivo]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RetiroEfectivo](
	[IDRetiroEfectivo] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NULL,
	[Concepto] [varchar](200) NULL,
	[Cantidad] [money] NULL,
 CONSTRAINT [PK_RetiroEfectivo] PRIMARY KEY CLUSTERED 
(
	[IDRetiroEfectivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Servicio]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Servicio](
	[IDServicio] [int] IDENTITY(1,1) NOT NULL,
	[IDTipoServicio] [int] NULL,
	[Descripcion] [varchar](70) NULL,
	[Precio] [money] NULL,
	[Fecha] [datetime] NULL,
	[Activo] [bit] NULL,
 CONSTRAINT [PK_Servicio] PRIMARY KEY CLUSTERED 
(
	[IDServicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ServicioCliente]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServicioCliente](
	[IDServicioCliente] [int] IDENTITY(1,1) NOT NULL,
	[Folio] [varchar](10) NULL,
	[IDCliente] [int] NULL,
	[IDServicio] [int] NULL,
	[IDCaja] [int] NULL,
	[Precio] [money] NULL,
	[Pendiente] [money] NULL,
 CONSTRAINT [PK_ServicioCliente] PRIMARY KEY CLUSTERED 
(
	[IDServicioCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoPago]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoPago](
	[IDTipoPago] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](30) NULL,
 CONSTRAINT [PK_TipoPago] PRIMARY KEY CLUSTERED 
(
	[IDTipoPago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoServicio]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoServicio](
	[IDTipoServicio] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](70) NOT NULL,
 CONSTRAINT [PK_TipoServicio] PRIMARY KEY CLUSTERED 
(
	[IDTipoServicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cliente] ON 

INSERT [dbo].[Cliente] ([IDCliente], [Nombre], [ApellidoPaterno], [ApellidoMaterno], [Telefono], [Celular], [CorreoElectronico], [Activo]) VALUES (1, N'RAQUEL', N'CHAVEZ', N'MUÑIZ', NULL, N'3318450404', NULL, 1)
SET IDENTITY_INSERT [dbo].[Cliente] OFF
SET IDENTITY_INSERT [dbo].[Servicio] ON 

INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (1, 1, N'LIMPIEZA FACIAL PROFUNDA', 450.0000, CAST(N'2021-11-23T19:39:40.450' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (2, 1, N'MICRODERMOABRASION CON PUNTA DIAMANTE', 450.0000, CAST(N'2021-11-23T19:39:40.450' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (3, 1, N'ANTI ACNE', 562.5000, CAST(N'2021-11-23T19:39:40.450' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (4, 1, N'FOTOREJUVENECIMIENTO  CON LUZ PULSADA', 562.5000, CAST(N'2021-11-23T19:39:40.450' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (5, 1, N'DESPIGMENTANTE CON LUZ PULSADA', 562.5000, CAST(N'2021-11-23T19:39:40.450' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (6, 1, N'DESPIGMENTANTE CON MICRODERMOABRASION', 562.5000, CAST(N'2021-11-23T19:39:40.450' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (7, 1, N'LIFTING FACIAL CON RADIOFRECUENCIA', 562.5000, CAST(N'2021-11-23T19:39:40.450' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (8, 1, N'RADIOFRECUENCIA BIPOLAR', 350.0000, CAST(N'2021-11-23T19:39:40.450' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (9, 1, N'RADIOFRECUENCIA TRIPOLAR', 350.0000, CAST(N'2021-11-23T19:39:40.450' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (10, 1, N'CARBOXITERAPIA FACIAL', 180.0000, CAST(N'2021-11-23T19:39:40.450' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (11, 2, N'LIPO SIN CIRUGIA', 500.0000, CAST(N'2021-11-30T20:00:00.667' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (12, 2, N'LEVANTAMIENTO DE GLUTEOS', 500.0000, CAST(N'2021-11-30T20:00:23.757' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (13, 2, N'ANTI CELULITIS', 500.0000, CAST(N'2021-11-30T20:00:45.150' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (14, 2, N'FLACIDEZ EN BRAZOS', 500.0000, CAST(N'2021-11-30T20:01:01.377' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (15, 2, N'TONIFICANTE CON ELECTRODOS', 250.0000, CAST(N'2021-11-30T20:02:17.340' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (16, 2, N'CARBOXITERAPIA FACIAL', 150.0000, CAST(N'2021-11-30T20:02:40.583' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (17, 2, N'CARBOXITERAPIA CORPORAL', 250.0000, CAST(N'2021-11-30T20:03:02.507' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (18, 2, N'POST QUIRURGICO', 450.0000, CAST(N'2021-11-30T20:04:15.173' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (19, 2, N'RADIO FRECUENCIA CORPORAL', 375.0000, CAST(N'2021-11-30T20:04:39.347' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (20, 3, N'LIPO SIN CIRUGIA', 2999.0000, CAST(N'2021-12-03T18:16:28.720' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (21, 3, N'LEVANTAMIENTO DE GLUTEOS', 2999.0000, CAST(N'2021-12-03T18:16:50.733' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (22, 3, N'ANTI CELULITIS', 2999.0000, CAST(N'2021-12-03T18:17:11.970' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (23, 3, N'FLACIDEZ EN BRAZOS', 2999.0000, CAST(N'2021-12-03T18:17:31.270' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (24, 3, N'TONIFICANTE CON ELECTRODOS', 2999.0000, CAST(N'2021-12-03T18:17:50.553' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (25, 3, N'CARBOXITERAPIA FACIAL', 2999.0000, CAST(N'2021-12-03T18:18:12.917' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (26, 3, N'CARBOXITERAPIA CORPORAL', 1999.0000, CAST(N'2021-12-03T18:18:38.927' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (27, 3, N'POST QUIRURGICO', 2999.0000, CAST(N'2021-12-03T18:19:07.043' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (28, 3, N'RADIO FRECUENCIA CORPORAL', 2599.0000, CAST(N'2021-12-03T18:19:27.810' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (29, 4, N'CUERPO COMPLETO DEPILACION INCLUYE ROSTRO', 1600.0000, CAST(N'2021-12-03T18:20:52.723' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (30, 4, N'CARA COMPLETA', 350.0000, CAST(N'2021-12-03T18:21:17.227' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (31, 4, N'BIGOTE', 130.0000, CAST(N'2021-12-03T18:21:44.417' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (32, 4, N'MENTON', 160.0000, CAST(N'2021-12-03T18:22:02.147' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (33, 4, N'PATILLAS', 150.0000, CAST(N'2021-12-03T18:44:31.850' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (34, 4, N'RESAQUE  AREA BIKINI', 250.0000, CAST(N'2021-12-03T18:45:07.820' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (35, 4, N'BRASILEÑO COMPLETO', 400.0000, CAST(N'2021-12-03T18:45:31.123' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (36, 4, N'BRAZOS COMPLETOS', 580.0000, CAST(N'2021-12-03T18:46:40.453' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (37, 4, N'MEDIOS BRAZOS', 580.0000, CAST(N'2021-12-03T18:47:10.533' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (38, 4, N'AXILAS', 580.0000, CAST(N'2021-12-03T18:47:26.420' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (39, 4, N'ABDOMEN', 580.0000, CAST(N'2021-12-03T18:47:39.603' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (40, 4, N'NUCA', 580.0000, CAST(N'2021-12-03T18:47:52.470' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (41, 4, N'ESPALDA SUPERIOR', 580.0000, CAST(N'2021-12-03T18:48:05.500' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (42, 4, N'ESPALDA INFERIOR', 580.0000, CAST(N'2021-12-03T18:48:21.200' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (43, 4, N'MEDIA PIERNA', 580.0000, CAST(N'2021-12-03T18:48:38.293' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (44, 4, N'PIERNAS COMPLETAS', 580.0000, CAST(N'2021-12-03T18:48:53.313' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (45, 4, N'LINEA INTERGLUTEA', 580.0000, CAST(N'2021-12-03T18:49:11.097' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (46, 4, N'GLUTEOS', 580.0000, CAST(N'2021-12-03T18:49:25.660' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (47, 4, N'PAQUETE 10 SESIONES', 580.0000, CAST(N'2021-12-03T18:49:46.000' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (48, 5, N'CUERPO COMPLETO  DEPILACION INCLUYE ROSTRO', 580.0000, CAST(N'2021-12-03T18:50:20.247' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (49, 5, N'ROSTRO COMPLETO', 580.0000, CAST(N'2021-12-03T18:50:37.157' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (50, 5, N'BARBA', 580.0000, CAST(N'2021-12-03T18:50:54.633' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (51, 5, N'MENTON', 580.0000, CAST(N'2021-12-03T18:51:08.780' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (52, 5, N'PECHO', 580.0000, CAST(N'2021-12-03T18:51:21.133' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (53, 5, N'ABDOMEN', 580.0000, CAST(N'2021-12-03T18:51:33.830' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (54, 5, N'RESAQUE', 580.0000, CAST(N'2021-12-03T18:51:48.333' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (55, 5, N'MEDIAS PIERNAS', 580.0000, CAST(N'2021-12-03T18:52:00.777' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (56, 5, N'PIERNAS COMPLETAS', 580.0000, CAST(N'2021-12-03T18:52:16.197' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (57, 5, N'PIES O MANOS', 580.0000, CAST(N'2021-12-03T18:52:34.657' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (58, 5, N'MEDIOS BRAZOS', 580.0000, CAST(N'2021-12-03T18:52:53.427' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (59, 5, N'BRAZOS COMPLETOS', 580.0000, CAST(N'2021-12-03T18:53:09.830' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (60, 5, N'MEDIA ESPALDA', 580.0000, CAST(N'2021-12-03T18:53:26.253' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (61, 5, N'ESPALDA BAJA', 580.0000, CAST(N'2021-12-03T18:53:41.717' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (62, 5, N'GLUTEOS', 580.0000, CAST(N'2021-12-03T18:53:56.313' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (63, 5, N'NUCA', 580.0000, CAST(N'2021-12-03T18:54:10.950' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (64, 5, N'PAQUETE 10 SESIONES', 580.0000, CAST(N'2021-12-03T18:54:29.540' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (65, 6, N'FRENTE', 580.0000, CAST(N'2021-12-03T18:55:03.990' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (66, 6, N'CEJA', 580.0000, CAST(N'2021-12-03T18:55:18.353' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (67, 6, N'PATILLAS', 580.0000, CAST(N'2021-12-03T19:06:50.697' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (68, 6, N'BIGOTE', 580.0000, CAST(N'2021-12-03T19:07:04.060' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (69, 6, N'MENTON', 580.0000, CAST(N'2021-12-03T19:07:19.397' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (70, 6, N'POMULOS', 580.0000, CAST(N'2021-12-03T19:07:33.447' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (71, 6, N'NUCA', 580.0000, CAST(N'2021-12-03T19:07:48.990' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (72, 6, N'ROSTRO COMPLETO', 580.0000, CAST(N'2021-12-03T19:08:01.597' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (73, 6, N'ABDOMEN', 580.0000, CAST(N'2021-12-03T19:08:15.133' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (74, 6, N'MEDIOS BRAZOS', 580.0000, CAST(N'2021-12-03T19:08:32.323' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (75, 6, N'BRAZOS COMPLETOS', 580.0000, CAST(N'2021-12-03T19:08:47.243' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (76, 6, N'AXILAS', 580.0000, CAST(N'2021-12-03T19:09:04.310' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (77, 6, N'ESPALDA BAJA', 580.0000, CAST(N'2021-12-03T19:09:21.233' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (78, 6, N'BIKINI RESAQUE', 580.0000, CAST(N'2021-12-03T19:09:39.473' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (79, 6, N'RESAQUE BRASILEÑO', 580.0000, CAST(N'2021-12-03T19:09:55.750' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (80, 6, N'LINEA INTERGLUTEA', 580.0000, CAST(N'2021-12-03T19:10:10.270' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (81, 6, N'GLUTEOS', 580.0000, CAST(N'2021-12-03T19:10:26.963' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (82, 6, N'MEDIAS PIERNAS', 580.0000, CAST(N'2021-12-03T19:10:40.267' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (83, 6, N'PIERNAS COMPLETAS', 580.0000, CAST(N'2021-12-03T19:10:54.877' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (84, 6, N'PIES O MANOS', 580.0000, CAST(N'2021-12-03T19:11:08.907' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (85, 6, N'CUERPO COMPLETO DEPILACION', 580.0000, CAST(N'2021-12-03T19:11:22.307' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (86, 7, N'RELAJANTE', 580.0000, CAST(N'2021-12-03T19:11:59.683' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (87, 7, N'DESCONTRACTURANTE', 580.0000, CAST(N'2021-12-03T19:12:12.170' AS DateTime), 1)
INSERT [dbo].[Servicio] ([IDServicio], [IDTipoServicio], [Descripcion], [Precio], [Fecha], [Activo]) VALUES (88, 7, N'DRENAJE LINFATICO', 580.0000, CAST(N'2021-12-03T19:12:30.320' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Servicio] OFF
SET IDENTITY_INSERT [dbo].[TipoPago] ON 

INSERT [dbo].[TipoPago] ([IDTipoPago], [Descripcion]) VALUES (1, N'EFECTIVO')
INSERT [dbo].[TipoPago] ([IDTipoPago], [Descripcion]) VALUES (2, N'TARJETA DE CREDITO')
INSERT [dbo].[TipoPago] ([IDTipoPago], [Descripcion]) VALUES (3, N'TARJETA DE DEBITO')
SET IDENTITY_INSERT [dbo].[TipoPago] OFF
SET IDENTITY_INSERT [dbo].[TipoServicio] ON 

INSERT [dbo].[TipoServicio] ([IDTipoServicio], [Descripcion]) VALUES (1, N'FACIALES')
INSERT [dbo].[TipoServicio] ([IDTipoServicio], [Descripcion]) VALUES (2, N'TRATAMIENTOS CORPORALES POR SESION')
INSERT [dbo].[TipoServicio] ([IDTipoServicio], [Descripcion]) VALUES (3, N'TRATAMIENTOS CORPORALES PAQUETE 10 SESIONES')
INSERT [dbo].[TipoServicio] ([IDTipoServicio], [Descripcion]) VALUES (4, N'DEPILACION CON LUZ PULSADA DAMA')
INSERT [dbo].[TipoServicio] ([IDTipoServicio], [Descripcion]) VALUES (5, N'DEPILACION CON LUZ PULSADA CABALLERO')
INSERT [dbo].[TipoServicio] ([IDTipoServicio], [Descripcion]) VALUES (6, N'DEPILACION CERA ESPAÑOLA POR ZONA')
INSERT [dbo].[TipoServicio] ([IDTipoServicio], [Descripcion]) VALUES (7, N'MASAJES')
SET IDENTITY_INSERT [dbo].[TipoServicio] OFF
ALTER TABLE [dbo].[Caja]  WITH CHECK ADD  CONSTRAINT [FK_Caja_Caja] FOREIGN KEY([IDIngresoEfectivo])
REFERENCES [dbo].[IngresoEfectivo] ([IDIngresoEfectivo])
GO
ALTER TABLE [dbo].[Caja] CHECK CONSTRAINT [FK_Caja_Caja]
GO
ALTER TABLE [dbo].[Caja]  WITH CHECK ADD  CONSTRAINT [FK_Caja_RetiroEfectivo] FOREIGN KEY([IDRetiroEfectivo])
REFERENCES [dbo].[RetiroEfectivo] ([IDRetiroEfectivo])
GO
ALTER TABLE [dbo].[Caja] CHECK CONSTRAINT [FK_Caja_RetiroEfectivo]
GO
ALTER TABLE [dbo].[IngresoEfectivo]  WITH CHECK ADD  CONSTRAINT [FK_IngresoEfectivo_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([IDCliente])
GO
ALTER TABLE [dbo].[IngresoEfectivo] CHECK CONSTRAINT [FK_IngresoEfectivo_Cliente]
GO
ALTER TABLE [dbo].[IngresoEfectivo]  WITH CHECK ADD  CONSTRAINT [FK_IngresoEfectivo_TipoPago] FOREIGN KEY([IDTipoPago])
REFERENCES [dbo].[TipoPago] ([IDTipoPago])
GO
ALTER TABLE [dbo].[IngresoEfectivo] CHECK CONSTRAINT [FK_IngresoEfectivo_TipoPago]
GO
ALTER TABLE [dbo].[PagoServicioCliente]  WITH CHECK ADD  CONSTRAINT [FK_PagoServicioCliente_IngresoEfectivo] FOREIGN KEY([IDIngresoEfectivo])
REFERENCES [dbo].[IngresoEfectivo] ([IDIngresoEfectivo])
GO
ALTER TABLE [dbo].[PagoServicioCliente] CHECK CONSTRAINT [FK_PagoServicioCliente_IngresoEfectivo]
GO
ALTER TABLE [dbo].[Servicio]  WITH CHECK ADD  CONSTRAINT [FK_Servicio_TipoServicio] FOREIGN KEY([IDTipoServicio])
REFERENCES [dbo].[TipoServicio] ([IDTipoServicio])
GO
ALTER TABLE [dbo].[Servicio] CHECK CONSTRAINT [FK_Servicio_TipoServicio]
GO
ALTER TABLE [dbo].[ServicioCliente]  WITH CHECK ADD  CONSTRAINT [FK_ServicioCliente_Caja] FOREIGN KEY([IDCaja])
REFERENCES [dbo].[Caja] ([IDCaja])
GO
ALTER TABLE [dbo].[ServicioCliente] CHECK CONSTRAINT [FK_ServicioCliente_Caja]
GO
ALTER TABLE [dbo].[ServicioCliente]  WITH CHECK ADD  CONSTRAINT [FK_ServicioCliente_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([IDCliente])
GO
ALTER TABLE [dbo].[ServicioCliente] CHECK CONSTRAINT [FK_ServicioCliente_Cliente]
GO
ALTER TABLE [dbo].[ServicioCliente]  WITH CHECK ADD  CONSTRAINT [FK_ServicioCliente_Servicio] FOREIGN KEY([IDServicio])
REFERENCES [dbo].[Servicio] ([IDServicio])
GO
ALTER TABLE [dbo].[ServicioCliente] CHECK CONSTRAINT [FK_ServicioCliente_Servicio]
GO
ALTER TABLE [dbo].[TipoPago]  WITH CHECK ADD  CONSTRAINT [FK_TipoPago_TipoPago] FOREIGN KEY([IDTipoPago])
REFERENCES [dbo].[TipoPago] ([IDTipoPago])
GO
ALTER TABLE [dbo].[TipoPago] CHECK CONSTRAINT [FK_TipoPago_TipoPago]
GO
/****** Object:  StoredProcedure [dbo].[sp_guardarIngresoEfectivo]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 12 de octubre de 2021
-- Description:	Permite ingresar un ingreso de efectivo, ya sea fisico o por tarjeta.
-- =============================================
CREATE PROCEDURE [dbo].[sp_guardarIngresoEfectivo] 
	@tipoPago AS VARCHAR(30),
	@IDCliente AS INT,
	@descripcionIngreso AS VARCHAR(200),
	@cantidad AS MONEY,
	@precio AS MONEY
AS
BEGIN
	DECLARE @idTipoPago AS INT,
			@saldoCajaActual AS MONEY,
			@msg AS VARCHAR(100),
			@IDIngresoEfectivo AS INT,
			@IDRetiroEfectivo AS INt,
			@IDCaja AS INT,
			@IDServicio AS INT,
			@IDServicioCliente AS INT,
			@SaldoPendiente AS MONEY

	SELECT @idTipoPago = IDTipoPago FROM TipoPago NOLOCK WHERE Descripcion = @tipoPago
	
	IF NOT EXISTS (SELECT 0 FROM Caja)
		BEGIN
			SET @saldoCajaActual = 0
		END
	ELSE 
		BEGIN
			SELECT TOP 1 @saldoCajaActual = Cantidad FROM Caja NOLOCK ORDER BY IDCaja DESC
		END
	SELECT TOP 1 @IDRetiroEfectivo = IDRetiroEfectivo FROM RetiroEfectivo NOLOCK ORDER BY IDRetiroEfectivo DESC
	SELECT TOP 1 @IDServicio = IDServicio FROM Servicio WHERE Descripcion = @descripcionIngreso AND Precio = @precio

	IF @precio = '0'
		BEGIN
			BEGIN TRANSACTION
				INSERT INTO IngresoEfectivo(IDTipoPago, IDCliente, Fecha, Concepto, Cantidad)
				VALUES(@idTipoPago, @IDCliente, GETDATE(), UPPER(@descripcionIngreso), @cantidad)

				SET @IDIngresoEfectivo = SCOPE_IDENTITY()

				IF @idTipoPago = 1
					BEGIN
						SET @saldoCajaActual = @saldoCajaActual + @cantidad

						INSERT INTO Caja(IDIngresoEfectivo, IDRetiroEfectivo, Cantidad)
						VALUES(@IDIngresoEfectivo, @IDRetiroEfectivo, @saldoCajaActual)

						SET @IDCaja = SCOPE_IDENTITY()
					END
				ELSE 
					BEGIN
						INSERT INTO Caja(IDIngresoEfectivo, IDRetiroEfectivo, Cantidad)
						VALUES(@IDIngresoEfectivo, @IDRetiroEfectivo, @saldoCajaActual)

						SET @IDCaja = SCOPE_IDENTITY()
					END
				IF @IDCliente <> 1
					BEGIN
						IF EXISTS (SELECT 0 FROM ServicioCliente NOLOCK WHERE Folio = @descripcionIngreso AND Pendiente > 0)
							BEGIN
								SELECT @SaldoPendiente = Pendiente FROM ServicioCliente NOLOCK WHERE Folio = @descripcionIngreso 

								IF @cantidad = @SaldoPendiente
									BEGIN
										UPDATE ServicioCliente
										SET Pendiente = 0
										WHERE Folio = @descripcionIngreso

										INSERT INTO PagoServicioCliente(IDIngresoEfectivo, FolioServicio, Pendiente)
										VALUES(@IDIngresoEfectivo, @descripcionIngreso, 0)
									END
								ELSE IF @cantidad > @SaldoPendiente
									BEGIN
										ROLLBACK TRANSACTION
										SET @msg = 'No esta permitido depositar mas de lo que tiene por liquidar del servicio.'
										RAISERROR (@msg, 16, 1)
									END 
								ELSE
									BEGIN
										SET @SaldoPendiente = @cantidad - @SaldoPendiente

										UPDATE ServicioCliente
										SET Pendiente = ABS(@SaldoPendiente)
										WHERE Folio = @descripcionIngreso

										INSERT INTO PagoServicioCliente(IDIngresoEfectivo, FolioServicio, Pendiente)
										VALUES(@IDIngresoEfectivo, @descripcionIngreso, ABS(@SaldoPendiente))
									END
							END
						ELSE
							BEGIN
								ROLLBACK TRANSACTION
								SET @msg = 'Este servicio ya fue liquidado o no existe.'
								RAISERROR (@msg, 16, 1)
							END 
					END
			COMMIT TRANSACTION

			SET @msg = 'El registro de ingreso se realizo correctamente.'
			SELECT @msg AS mensaje
		END
	ELSE 
		BEGIN
			BEGIN TRANSACTION
				INSERT INTO IngresoEfectivo(IDTipoPago, IDCliente, Fecha, Concepto, Cantidad)
				VALUES(@idTipoPago, @IDCliente, GETDATE(), UPPER(@descripcionIngreso), @cantidad)

				SET @IDIngresoEfectivo = SCOPE_IDENTITY()

				IF @idTipoPago = 1
					BEGIN
						SET @saldoCajaActual = @saldoCajaActual + @cantidad

						INSERT INTO Caja(IDIngresoEfectivo, IDRetiroEfectivo, Cantidad)
						VALUES(@IDIngresoEfectivo, @IDRetiroEfectivo, @saldoCajaActual)

						SET @IDCaja = SCOPE_IDENTITY()
					END
				ELSE 
					BEGIN
						INSERT INTO Caja(IDIngresoEfectivo, IDRetiroEfectivo, Cantidad)
						VALUES(@IDIngresoEfectivo, @IDRetiroEfectivo, @saldoCajaActual)

						SET @IDCaja = SCOPE_IDENTITY()
					END

				IF @cantidad <= @precio
					BEGIN
						INSERT INTO ServicioCliente(IDCliente, IDServicio, IDCaja, Precio, Pendiente)
						VALUES(@IDCliente, @IDServicio, @IDCaja, @precio, ABS(@cantidad - @precio))

						SET @IDServicioCliente = SCOPE_IDENTITY()

						UPDATE ServicioCliente
						SET Folio = REPLICATE('0', 10 - LEN(@IDServicioCliente)) + CAST(@IDServicioCliente AS VARCHAR)
						WHERE IDServicioCliente = @IDServicioCliente

						INSERT INTO PagoServicioCliente(IDIngresoEfectivo, FolioServicio, Pendiente)
						VALUES(@IDIngresoEfectivo, REPLICATE('0', 10 - LEN(@IDServicioCliente)) + CAST(@IDServicioCliente AS VARCHAR), ABS(@cantidad - @precio))
					END
				ELSE IF @cantidad > @precio
					BEGIN
						ROLLBACK TRANSACTION
						SET @msg = 'No esta permitido depositar mas de lo que tiene por liquidar del servicio.'
						RAISERROR (@msg, 16, 1)
					END 
				ELSE
					BEGIN
						INSERT INTO ServicioCliente(IDCliente, IDServicio, IDCaja, Precio, Pendiente)
						VALUES(@IDCliente, @IDServicio, @IDCaja, @precio, 0)

						SET @IDServicioCliente = SCOPE_IDENTITY()

						UPDATE ServicioCliente
						SET Folio = REPLICATE('0', 10 - LEN(@IDServicioCliente)) + CAST(@IDServicioCliente AS VARCHAR)
						WHERE IDServicioCliente = @IDServicioCliente

						INSERT INTO PagoServicioCliente(IDIngresoEfectivo, FolioServicio, Pendiente)
						VALUES(@IDIngresoEfectivo, REPLICATE('0', 10 - LEN(@IDServicioCliente)) + CAST(@IDServicioCliente AS VARCHAR), 0)
					END
			COMMIT TRANSACTION

			SET @msg = 'El registro de ingreso se realizo correctamente.'
			SELECT @msg AS mensaje
		END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_guardarNuevoCliente]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz Gonzalez
-- Create date: 22 de octubre de 2021
-- Description:	Permite guardar un nuevo cliente con un saldo inicial
-- =============================================
CREATE PROCEDURE [dbo].[sp_guardarNuevoCliente]
	@nombre AS VARCHAR(50),
	@apellidoPaterno AS VARCHAR(30),
	@apellidoMaterno AS VARCHAR(30),
	@telefomo AS VARCHAR(10),
	@celular AS VARCHAR(10),
	@correoElectronico AS VARCHAR(50)
AS
	BEGIN
		DECLARE @msg varchar(100),
				@IDCliente AS INT,
				@IDCaja AS MONEY

		IF EXISTS(SELECT 0 FROM Cliente WHERE Nombre = UPPER(@nombre) AND ApellidoPaterno = UPPER(@apellidoPaterno) AND ApellidoMaterno = UPPER(@apellidoMaterno))
			BEGIN
				SET @msg = 'El cliente ya se encuentra registrado.'
				RAISERROR (@msg, 16, 1)
			END
		ELSE
			BEGIN
				BEGIN TRANSACTION
					INSERT INTO Cliente(Nombre, ApellidoPaterno, ApellidoMaterno, Telefono, Celular, CorreoElectronico, Activo)
					VALUES(UPPER(@nombre), UPPER(@apellidoPaterno), UPPER(@apellidoMaterno), @telefomo, @celular, UPPER(@correoElectronico), 1)
				COMMIT TRANSACTION

				SET @msg = 'El registro se realizo correctamente.'
				SELECT @msg AS Mensaje
			END
	END
GO
/****** Object:  StoredProcedure [dbo].[sp_guardarRetiroEfectivo]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz Gonzalezc
-- Description:	Permite guardar los registros de reiro en efectivo
-- =============================================
CREATE PROCEDURE [dbo].[sp_guardarRetiroEfectivo] 
	@tipoPago AS VARCHAR(30),
	@concepto AS VARCHAR(200),
	@cantidad MONEY
AS
BEGIN
	DECLARE @idTipoPago AS INT,
			@saldoCajaActual AS MONEY,
			@msg AS VARCHAR(100),
			@IDIngresoEfectivo AS INT,
			@IDRetiroEfectivo AS INT

	SELECT @idTipoPago = IDTipoPago FROM TipoPago NOLOCK WHERE Descripcion = @tipoPago
	SELECT TOP 1 @saldoCajaActual = Cantidad FROM Caja NOLOCK ORDER BY IDCaja DESC
	SELECT TOP 1 @IDIngresoEfectivo = IDIngresoEfectivo FROM IngresoEfectivo NOLOCK ORDER BY IDIngresoEfectivo DESC

	BEGIN TRANSACTION
		SET @saldoCajaActual = @saldoCajaActual - @cantidad

		INSERT INTO RetiroEfectivo(Fecha, Concepto, Cantidad)
		VALUES(GETDATE(), UPPER(@concepto), @cantidad)

		SET @IDRetiroEfectivo = SCOPE_IDENTITY()

		INSERT INTO Caja(IDIngresoEfectivo, IDRetiroEfectivo, Cantidad)
		VALUES(@IDIngresoEfectivo, @IDRetiroEfectivo, @saldoCajaActual)
	COMMIT TRANSACTION

	SET @msg = 'El registro se realizo correctamente.'
	SELECT @msg AS Mensaje
END
GO
/****** Object:  StoredProcedure [dbo].[sp_modificarInfoCliente]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 29 de noviembre de 2021
-- Description:	Permite editar la informacion de un cliente.
-- =============================================
CREATE PROCEDURE [dbo].[sp_modificarInfoCliente]
	@idCliente AS INT,
	@nombre VARCHAR(50) = NULL,
	@apellidoPaterno AS VARCHAR(30) = NULL,
	@apellidoMaterno AS VARCHAR(30) = NULL,
	@telefono AS VARCHAR(10) = NULL,
	@celular AS VARCHAR(10) = NULL,
	@correo AS VARCHAR(50) = NULL
AS
BEGIN
	BEGIN TRANSACTION
		UPDATE 
			Cliente
		SET		
			Nombre = ISNULL(UPPER(@nombre), Nombre),
			ApellidoPaterno = ISNULL(UPPER(@apellidoPaterno), ApellidoPaterno),
			ApellidoMaterno = ISNULL(UPPER(@apellidoMaterno), ApellidoMaterno),
			Telefono = ISNULL(@telefono, Telefono),
			Celular = ISNULL(@celular, Celular),
			CorreoElectronico = ISNULL(UPPER(@correo), CorreoElectronico)
		WHERE
			IDCliente = @idCliente
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[sp_modificarPrecioServicio]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz Gonzalez
-- Create date: 13 de noviembre de 2021
-- Description:	Permite modificar el precio de un servicio
-- =============================================
CREATE PROCEDURE [dbo].[sp_modificarPrecioServicio] 
	@IDServicio AS INT,
	@PrecioNuevo AS MONEY
AS
BEGIN
	BEGIN TRANSACTION
		UPDATE Servicio
		SET Precio = @PrecioNuevo,
			Fecha = GETDATE()
		WHERE IDServicio = @IDServicio
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[sp_mostrarCaja]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 10 de octubre de 2021
-- Description:	Muestra los registros de la tabla Caja
-- =============================================
CREATE PROCEDURE [dbo].[sp_mostrarCaja] 
AS
BEGIN
	
	SELECT
		a.IDCaja,
		e.Descripcion AS TipoPago,
		ISNULL(d.Nombre + ' ' + d.ApellidoPaterno + ' ' + d.ApellidoMaterno, 'N/A') AS NombreCliente,
		COALESCE(CONVERT(VARCHAR(255), b.Fecha), 'N/A') AS FechaIngreso,
		ISNULL(b.Concepto, 'N/A') AS DescripcionIngreso,
		ISNULL(b.Cantidad, 0.00) AS CantidadIngreso,
		ISNULL(c.Concepto, 'N/A') AS ConceptoRetiro,
		ISNULL(c.Cantidad, 0.00) AS CantidadRetiro,
		COALESCE(CONVERT(VARCHAR(255), c.Fecha), 'N/A') AS FechaRetiro,
		a.Cantidad AS Caja
	FROM 
		Caja AS a
		LEFT OUTER JOIN IngresoEfectivo AS b
		ON a.IDIngresoEfectivo = b.IDIngresoEfectivo
		LEFT OUTER JOIN RetiroEfectivo AS c
		ON c.IDRetiroEfectivo = a.IDRetiroEfectivo
		LEFT OUTER JOIN Cliente AS d
		ON d.IDCliente = b.IDCliente
		LEFT OUTER JOIN TipoPago AS e
		ON e.IDTipoPago = b.IDTipoPago
	ORDER BY a.IDCaja DESC
END
GO
/****** Object:  StoredProcedure [dbo].[sp_mostrarClientes]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesus Muñiz Gonzalez
-- Create date: 09/octubre/2021
-- Description:	Devuelve todos los registros de clientes.
-- =============================================
CREATE PROCEDURE [dbo].[sp_mostrarClientes] 

AS
BEGIN
	SELECT 
		IDCliente,
		Nombre,
		ApellidoPaterno,
		ApellidoMaterno,
		ISNULL(Telefono, 'N/A') AS Telefono,
		Celular,
		ISNULL(CorreoElectronico, 'N/A') AS CorreoElectronico,
		Activo
	FROM 
		Cliente
	WHERE
		Activo = 1
	ORDER BY IDCliente DESC
END
GO
/****** Object:  StoredProcedure [dbo].[sp_mostrarServicioCliente]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús muñiz Gonzalez
-- Create date: 13 de noviembre de 2021
-- Description:	Muestra los servicios contratados por cliente
-- =============================================
CREATE PROCEDURE [dbo].[sp_mostrarServicioCliente] 
	@IDCLiente AS INT
AS
BEGIN
	SELECT 
		a.IDServicioCliente AS IDServicioCliente,
		b.IDCliente AS IDCliente,
		c.IDServicio AS IDServicio,
		a.Folio AS Folio,
		d.Descripcion AS TipoServicio,
		c.Descripcion AS Servicio,
		a.Precio AS Precio,
		a.Pendiente AS SaldoPendiente,
		f.Fecha AS Fecha
	FROM
		ServicioCliente AS a
		LEFT OUTER JOIN Cliente AS b
		ON a.IDCliente = b.IDCliente
		LEFT OUTER JOIN Servicio AS c
		ON c.IDServicio = a.IDServicio
		LEFT OUTER JOIN TipoServicio AS d
		ON d.IDTipoServicio = c.IDTipoServicio
		LEFT OUTER JOIN Caja AS e
		ON e.IDCaja = a.IDCaja
		LEFT OUTER JOIN IngresoEfectivo AS f
		ON f.IDIngresoEfectivo = e.IDIngresoEfectivo
	WHERE
		b.IDCliente = @IDCLiente
	ORDER BY 
		a.IDServicioCliente DESC
END
GO
/****** Object:  StoredProcedure [dbo].[sp_mostrarServicios]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 07 de noviembre de 2021
-- Description:	Permite mostrar la lista de servicios disponibles
-- =============================================
CREATE PROCEDURE [dbo].[sp_mostrarServicios]

AS
BEGIN
	SELECT 
		a.IDServicio AS IDServicio,
		b.Descripcion AS TipoServicio,
		a.Descripcion AS Servicio,
		a.Precio AS Precio,
		a.Fecha AS Fecha
	FROM 
		Servicio AS a
		LEFT OUTER JOIN TipoServicio AS b
		ON a.IDTipoServicio = b.IDTipoServicio
	WHERE
		a.Activo = 1
	ORDER BY b.IDTipoServicio DESC
END
GO
/****** Object:  StoredProcedure [dbo].[sp_mostrarTipoPago]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 10 de octubre de 2021
-- Description:	Muestra los tipos de pago existentes
-- =============================================
CREATE PROCEDURE [dbo].[sp_mostrarTipoPago] 
AS
BEGIN
	SELECT
		IDTipoPago,
		Descripcion
	FROM 
		TipoPago
END
GO
/****** Object:  StoredProcedure [dbo].[sp_obtenerInfoTicket]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 22 de noviembre de 2021
-- Description:	Permite obtener la informacion del ticket de pago que realizo el cliente.
-- =============================================
CREATE PROCEDURE [dbo].[sp_obtenerInfoTicket] 
	@IDCaja AS INT
AS
BEGIN
	DECLARE @IDIngresoEfectivo AS INT,
			@Folio AS VARCHAR(10),
			@cantidadIngreso AS MONEY,
			@nombreCliente VARCHAR(80),
			@IDPagoServicioCliente AS INT,
			@fecha AS DATETIME

	SELECT 
		@IDIngresoEfectivo = b.IDIngresoEfectivo,
		@cantidadIngreso = b.Cantidad,
		@nombreCliente = c.Nombre + ' ' + c.ApellidoPaterno + ' ' + c.ApellidoMaterno,
		@IDPagoServicioCliente = d.IDPagoServicioCliente,
		@fecha = b.Fecha
	FROM 
		Caja AS a
		LEFT OUTER JOIN IngresoEfectivo AS b
		ON a.IDIngresoEfectivo = b.IDIngresoEfectivo
		LEFT OUTER JOIN Cliente AS c
		ON c.IDCliente = b.IDCliente
		LEFT OUTER JOIN PagoServicioCliente AS d
		ON d.IDIngresoEfectivo = b.IDIngresoEfectivo
	WHERE 
		a.IDCaja = @IDCaja

	IF EXISTS(SELECT 0 FROM ServicioCliente WHERE IDCaja = @IDCaja)
		BEGIN
			SELECT 
				a.Folio AS Folio,
				b.Descripcion AS Servicio,
				a.Precio AS Precio,
				@cantidadIngreso AS Importe,
				e.Pendiente AS SaldoPendiente,
				@nombreCliente AS NombreCliente,
				@IDPagoServicioCliente AS IDPagoServicioCliente,
				@fecha AS Fecha
			FROM	
				ServicioCliente AS a
				LEFT OUTER JOIN Servicio AS b
				ON a.IDServicio = b.IDServicio
				LEFT OUTER JOIN Caja AS c
				ON c.IDCaja = a.IDCaja
				LEFT OUTER JOIN IngresoEfectivo AS d
				ON d.IDIngresoEfectivo = c.IDIngresoEfectivo
				LEFT OUTER JOIN PagoServicioCliente AS e
				ON e.IDIngresoEfectivo = d.IDIngresoEfectivo
			WHERE
				a.IDCaja = @IDCaja
		END
	ELSE
		BEGIN
			SELECT @Folio = FolioServicio FROM PagoServicioCliente WHERE IDIngresoEfectivo = @IDIngresoEfectivo

			SELECT
				c.Folio AS Folio,
				d.Descripcion AS Servicio,
				c.Precio AS Precio,
				@cantidadIngreso AS Importe,
				a.Pendiente AS SaldoPendiente,
				@nombreCliente AS NombreCliente,
				a.IDPagoServicioCliente AS IDPagoServicioCliente,
				b.Fecha AS Fecha
			FROM
				PagoServicioCliente AS a
				LEFT OUTER JOIN IngresoEfectivo AS b
				ON a.IDIngresoEfectivo = b.IDIngresoEfectivo
				LEFT OUTER JOIN ServicioCliente AS c
				ON c.Folio = a.FolioServicio
				LEFT OUTER JOIN Servicio AS d
				ON d.IDServicio = c.IDServicio
			WHERE
				a.IDIngresoEfectivo = @IDIngresoEfectivo
		END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_obtenerNombreCliente]    Script Date: 03/12/2021 08:16:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 19 de noviembre de 2021
-- Description:	Obtiene el nombre completo de un cliente mediante su ID.
-- =============================================
CREATE PROCEDURE [dbo].[sp_obtenerNombreCliente] 
	@IDCliente AS INTEGER
AS
BEGIN
	SELECT 
		Nombre + ' ' + ApellidoPaterno + ' ' + ApellidoMaterno AS NombreCliente
	FROM
		Cliente
	WHERE
		IDCliente = @idCliente
END
GO
