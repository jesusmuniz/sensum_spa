USE [Sensum]
GO
/****** Object:  StoredProcedure [dbo].[sp_mostrarClientes]    Script Date: 19/11/2021 11:48:06 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesus Muñiz Gonzalez
-- Create date: 09/octubre/2021
-- Description:	Devuelve todos los registros de clientes.
-- =============================================
ALTER PROCEDURE [dbo].[sp_mostrarClientes] 

AS
BEGIN
	SELECT 
		IDCliente,
		Nombre,
		ApellidoPaterno,
		ApellidoMaterno,
		ISNULL(Telefono, 'N/A') AS Telefono,
		Celular,
		ISNULL(CorreoElectronico, 'N/A') AS CorreoElectronico,
		Activo
	FROM 
		Cliente
	WHERE
		Activo = 1
	ORDER BY IDCliente DESC
END
