USE [Sensum]
GO
/****** Object:  StoredProcedure [dbo].[sp_modificarPrecioServicio]    Script Date: 23/11/2021 07:38:30 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz Gonzalez
-- Create date: 13 de noviembre de 2021
-- Description:	Permite modificar el precio de un servicio
-- =============================================
ALTER PROCEDURE [dbo].[sp_modificarPrecioServicio] 
	@IDServicio AS INT,
	@PrecioNuevo AS MONEY
AS
BEGIN
	BEGIN TRANSACTION
		UPDATE Servicio
		SET Precio = @PrecioNuevo,
			Fecha = GETDATE()
		WHERE IDServicio = @IDServicio
	COMMIT TRANSACTION
END
