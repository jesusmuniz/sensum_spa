USE [Sensum]
GO
/****** Object:  StoredProcedure [dbo].[sp_obtenerInfoTicket]    Script Date: 29/11/2021 06:11:54 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 22 de noviembre de 2021
-- Description:	Permite obtener la informacion del ticket de pago que realizo el cliente.
-- =============================================
ALTER PROCEDURE [dbo].[sp_obtenerInfoTicket] 
	@IDCaja AS INT
AS
BEGIN
	DECLARE @IDIngresoEfectivo AS INT,
			@Folio AS VARCHAR(10),
			@cantidadIngreso AS MONEY,
			@nombreCliente VARCHAR(80),
			@IDPagoServicioCliente AS INT,
			@fecha AS DATETIME

	SELECT 
		@IDIngresoEfectivo = b.IDIngresoEfectivo,
		@cantidadIngreso = b.Cantidad,
		@nombreCliente = c.Nombre + ' ' + c.ApellidoPaterno + ' ' + c.ApellidoMaterno,
		@IDPagoServicioCliente = d.IDPagoServicioCliente,
		@fecha = b.Fecha
	FROM 
		Caja AS a
		LEFT OUTER JOIN IngresoEfectivo AS b
		ON a.IDIngresoEfectivo = b.IDIngresoEfectivo
		LEFT OUTER JOIN Cliente AS c
		ON c.IDCliente = b.IDCliente
		LEFT OUTER JOIN PagoServicioCliente AS d
		ON d.IDIngresoEfectivo = b.IDIngresoEfectivo
	WHERE 
		a.IDCaja = @IDCaja

	IF EXISTS(SELECT 0 FROM ServicioCliente WHERE IDCaja = @IDCaja)
		BEGIN
			SELECT 
				a.Folio AS Folio,
				b.Descripcion AS Servicio,
				a.Precio AS Precio,
				@cantidadIngreso AS Importe,
				e.Pendiente AS SaldoPendiente,
				@nombreCliente AS NombreCliente,
				@IDPagoServicioCliente AS IDPagoServicioCliente,
				@fecha AS Fecha
			FROM	
				ServicioCliente AS a
				LEFT OUTER JOIN Servicio AS b
				ON a.IDServicio = b.IDServicio
				LEFT OUTER JOIN Caja AS c
				ON c.IDCaja = a.IDCaja
				LEFT OUTER JOIN IngresoEfectivo AS d
				ON d.IDIngresoEfectivo = c.IDIngresoEfectivo
				LEFT OUTER JOIN PagoServicioCliente AS e
				ON e.IDIngresoEfectivo = d.IDIngresoEfectivo
			WHERE
				a.IDCaja = @IDCaja
		END
	ELSE
		BEGIN
			SELECT @Folio = FolioServicio FROM PagoServicioCliente WHERE IDIngresoEfectivo = @IDIngresoEfectivo

			SELECT
				c.Folio AS Folio,
				d.Descripcion AS Servicio,
				c.Precio AS Precio,
				@cantidadIngreso AS Importe,
				a.Pendiente AS SaldoPendiente,
				@nombreCliente AS NombreCliente,
				a.IDPagoServicioCliente AS IDPagoServicioCliente,
				b.Fecha AS Fecha
			FROM
				PagoServicioCliente AS a
				LEFT OUTER JOIN IngresoEfectivo AS b
				ON a.IDIngresoEfectivo = b.IDIngresoEfectivo
				LEFT OUTER JOIN ServicioCliente AS c
				ON c.Folio = a.FolioServicio
				LEFT OUTER JOIN Servicio AS d
				ON d.IDServicio = c.IDServicio
			WHERE
				a.IDIngresoEfectivo = @IDIngresoEfectivo
		END
END
