USE [Sensum]
GO
/****** Object:  StoredProcedure [dbo].[sp_guardarIngresoEfectivo]    Script Date: 26/11/2021 10:40:06 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 12 de octubre de 2021
-- Description:	Permite ingresar un ingreso de efectivo, ya sea fisico o por tarjeta.
-- =============================================
ALTER PROCEDURE [dbo].[sp_guardarIngresoEfectivo] 
	@tipoPago AS VARCHAR(30),
	@IDCliente AS INT,
	@descripcionIngreso AS VARCHAR(200),
	@cantidad AS MONEY,
	@precio AS MONEY
AS
BEGIN
	DECLARE @idTipoPago AS INT,
			@saldoCajaActual AS MONEY,
			@msg AS VARCHAR(100),
			@IDIngresoEfectivo AS INT,
			@IDRetiroEfectivo AS INt,
			@IDCaja AS INT,
			@IDServicio AS INT,
			@IDServicioCliente AS INT,
			@SaldoPendiente AS MONEY

	SELECT @idTipoPago = IDTipoPago FROM TipoPago NOLOCK WHERE Descripcion = @tipoPago
	
	IF NOT EXISTS (SELECT 0 FROM Caja)
		BEGIN
			SET @saldoCajaActual = 0
		END
	ELSE 
		BEGIN
			SELECT TOP 1 @saldoCajaActual = Cantidad FROM Caja NOLOCK ORDER BY IDCaja DESC
		END
	SELECT TOP 1 @IDRetiroEfectivo = IDRetiroEfectivo FROM RetiroEfectivo NOLOCK ORDER BY IDRetiroEfectivo DESC
	SELECT TOP 1 @IDServicio = IDServicio FROM Servicio WHERE Descripcion = @descripcionIngreso AND Precio = @precio

	IF @precio = '0'
		BEGIN
			BEGIN TRANSACTION
				INSERT INTO IngresoEfectivo(IDTipoPago, IDCliente, Fecha, Concepto, Cantidad)
				VALUES(@idTipoPago, @IDCliente, GETDATE(), UPPER(@descripcionIngreso), @cantidad)

				SET @IDIngresoEfectivo = SCOPE_IDENTITY()

				IF @idTipoPago = 1
					BEGIN
						SET @saldoCajaActual = @saldoCajaActual + @cantidad

						INSERT INTO Caja(IDIngresoEfectivo, IDRetiroEfectivo, Cantidad)
						VALUES(@IDIngresoEfectivo, @IDRetiroEfectivo, @saldoCajaActual)

						SET @IDCaja = SCOPE_IDENTITY()
					END
				ELSE 
					BEGIN
						INSERT INTO Caja(IDIngresoEfectivo, IDRetiroEfectivo, Cantidad)
						VALUES(@IDIngresoEfectivo, @IDRetiroEfectivo, @saldoCajaActual)

						SET @IDCaja = SCOPE_IDENTITY()
					END
				IF @IDCliente <> 1
					BEGIN
						IF EXISTS (SELECT 0 FROM ServicioCliente NOLOCK WHERE Folio = @descripcionIngreso AND Pendiente > 0)
							BEGIN
								SELECT @SaldoPendiente = Pendiente FROM ServicioCliente NOLOCK WHERE Folio = @descripcionIngreso 

								IF @cantidad = @SaldoPendiente
									BEGIN
										UPDATE ServicioCliente
										SET Pendiente = 0
										WHERE Folio = @descripcionIngreso

										INSERT INTO PagoServicioCliente(IDIngresoEfectivo, FolioServicio, Pendiente)
										VALUES(@IDIngresoEfectivo, @descripcionIngreso, 0)
									END
								ELSE IF @cantidad > @SaldoPendiente
									BEGIN
										ROLLBACK TRANSACTION
										SET @msg = 'No esta permitido depositar mas de lo que tiene por liquidar del servicio.'
										RAISERROR (@msg, 16, 1)
									END 
								ELSE
									BEGIN
										SET @SaldoPendiente = @cantidad - @SaldoPendiente

										UPDATE ServicioCliente
										SET Pendiente = ABS(@SaldoPendiente)
										WHERE Folio = @descripcionIngreso

										INSERT INTO PagoServicioCliente(IDIngresoEfectivo, FolioServicio, Pendiente)
										VALUES(@IDIngresoEfectivo, @descripcionIngreso, ABS(@SaldoPendiente))
									END
							END
						ELSE
							BEGIN
								ROLLBACK TRANSACTION
								SET @msg = 'Este servicio ya fue liquidado o no existe.'
								RAISERROR (@msg, 16, 1)
							END 
					END
			COMMIT TRANSACTION

			SET @msg = 'El registro de ingreso se realizo correctamente.'
			SELECT @msg AS mensaje
		END
	ELSE 
		BEGIN
			BEGIN TRANSACTION
				INSERT INTO IngresoEfectivo(IDTipoPago, IDCliente, Fecha, Concepto, Cantidad)
				VALUES(@idTipoPago, @IDCliente, GETDATE(), UPPER(@descripcionIngreso), @cantidad)

				SET @IDIngresoEfectivo = SCOPE_IDENTITY()

				IF @idTipoPago = 1
					BEGIN
						SET @saldoCajaActual = @saldoCajaActual + @cantidad

						INSERT INTO Caja(IDIngresoEfectivo, IDRetiroEfectivo, Cantidad)
						VALUES(@IDIngresoEfectivo, @IDRetiroEfectivo, @saldoCajaActual)

						SET @IDCaja = SCOPE_IDENTITY()
					END
				ELSE 
					BEGIN
						INSERT INTO Caja(IDIngresoEfectivo, IDRetiroEfectivo, Cantidad)
						VALUES(@IDIngresoEfectivo, @IDRetiroEfectivo, @saldoCajaActual)

						SET @IDCaja = SCOPE_IDENTITY()
					END

				IF @cantidad <= @precio
					BEGIN
						INSERT INTO ServicioCliente(IDCliente, IDServicio, IDCaja, Precio, Pendiente)
						VALUES(@IDCliente, @IDServicio, @IDCaja, @precio, ABS(@cantidad - @precio))

						SET @IDServicioCliente = SCOPE_IDENTITY()

						UPDATE ServicioCliente
						SET Folio = REPLICATE('0', 10 - LEN(@IDServicioCliente)) + CAST(@IDServicioCliente AS VARCHAR)
						WHERE IDServicioCliente = @IDServicioCliente

						INSERT INTO PagoServicioCliente(IDIngresoEfectivo, FolioServicio, Pendiente)
						VALUES(@IDIngresoEfectivo, REPLICATE('0', 10 - LEN(@IDServicioCliente)) + CAST(@IDServicioCliente AS VARCHAR), ABS(@cantidad - @precio))
					END
				ELSE IF @cantidad > @precio
					BEGIN
						ROLLBACK TRANSACTION
						SET @msg = 'No esta permitido depositar mas de lo que tiene por liquidar del servicio.'
						RAISERROR (@msg, 16, 1)
					END 
				ELSE
					BEGIN
						INSERT INTO ServicioCliente(IDCliente, IDServicio, IDCaja, Precio, Pendiente)
						VALUES(@IDCliente, @IDServicio, @IDCaja, @precio, 0)

						SET @IDServicioCliente = SCOPE_IDENTITY()

						UPDATE ServicioCliente
						SET Folio = REPLICATE('0', 10 - LEN(@IDServicioCliente)) + CAST(@IDServicioCliente AS VARCHAR)
						WHERE IDServicioCliente = @IDServicioCliente

						INSERT INTO PagoServicioCliente(IDIngresoEfectivo, FolioServicio, Pendiente)
						VALUES(@IDIngresoEfectivo, REPLICATE('0', 10 - LEN(@IDServicioCliente)) + CAST(@IDServicioCliente AS VARCHAR), 0)
					END
			COMMIT TRANSACTION

			SET @msg = 'El registro de ingreso se realizo correctamente.'
			SELECT @msg AS mensaje
		END
END
