USE Sensum
GO
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jes�s Mu�iz Gonz�lez
-- Create date: 29 de noviembre de 2021
-- Description:	Permite editar la informacion de un cliente.
-- =============================================
ALTER PROCEDURE sp_modificarInfoCliente
	@idCliente AS INT,
	@nombre VARCHAR(50) = NULL,
	@apellidoPaterno AS VARCHAR(30) = NULL,
	@apellidoMaterno AS VARCHAR(30) = NULL,
	@telefono AS VARCHAR(10) = NULL,
	@celular AS VARCHAR(10) = NULL,
	@correo AS VARCHAR(50) = NULL
AS
BEGIN
	BEGIN TRANSACTION
		UPDATE 
			Cliente
		SET		
			Nombre = ISNULL(UPPER(@nombre), Nombre),
			ApellidoPaterno = ISNULL(UPPER(@apellidoPaterno), ApellidoPaterno),
			ApellidoMaterno = ISNULL(UPPER(@apellidoMaterno), ApellidoMaterno),
			Telefono = ISNULL(@telefono, Telefono),
			Celular = ISNULL(@celular, Celular),
			CorreoElectronico = ISNULL(UPPER(@correo), CorreoElectronico)
		WHERE
			IDCliente = @idCliente
	COMMIT TRANSACTION
END
GO
