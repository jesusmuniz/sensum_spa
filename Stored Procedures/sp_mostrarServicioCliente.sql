USE [Sensum]
GO
/****** Object:  StoredProcedure [dbo].[sp_mostrarServicioCliente]    Script Date: 29/11/2021 08:15:55 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús muñiz Gonzalez
-- Create date: 13 de noviembre de 2021
-- Description:	Muestra los servicios contratados por cliente
-- =============================================
ALTER PROCEDURE [dbo].[sp_mostrarServicioCliente] 
	@IDCLiente AS INT
AS
BEGIN
	SELECT 
		a.IDServicioCliente AS IDServicioCliente,
		b.IDCliente AS IDCliente,
		c.IDServicio AS IDServicio,
		a.Folio AS Folio,
		d.Descripcion AS TipoServicio,
		c.Descripcion AS Servicio,
		a.Precio AS Precio,
		a.Pendiente AS SaldoPendiente,
		f.Fecha AS Fecha
	FROM
		ServicioCliente AS a
		LEFT OUTER JOIN Cliente AS b
		ON a.IDCliente = b.IDCliente
		LEFT OUTER JOIN Servicio AS c
		ON c.IDServicio = a.IDServicio
		LEFT OUTER JOIN TipoServicio AS d
		ON d.IDTipoServicio = c.IDTipoServicio
		LEFT OUTER JOIN Caja AS e
		ON e.IDCaja = a.IDCaja
		LEFT OUTER JOIN IngresoEfectivo AS f
		ON f.IDIngresoEfectivo = e.IDIngresoEfectivo
	WHERE
		b.IDCliente = @IDCLiente
	ORDER BY 
		a.IDServicioCliente DESC
END
