USE [Sensum]
GO
/****** Object:  StoredProcedure [dbo].[sp_mostrarCaja]    Script Date: 19/11/2021 11:47:46 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jesús Muñiz González
-- Create date: 10 de octubre de 2021
-- Description:	Muestra los registros de la tabla Caja
-- =============================================
ALTER PROCEDURE [dbo].[sp_mostrarCaja] 
AS
BEGIN
	
	SELECT
		a.IDCaja,
		e.Descripcion AS TipoPago,
		ISNULL(d.Nombre + ' ' + d.ApellidoPaterno + ' ' + d.ApellidoMaterno, 'N/A') AS NombreCliente,
		COALESCE(CONVERT(VARCHAR(255), b.Fecha), 'N/A') AS FechaIngreso,
		ISNULL(b.Concepto, 'N/A') AS DescripcionIngreso,
		ISNULL(b.Cantidad, 0.00) AS CantidadIngreso,
		ISNULL(c.Concepto, 'N/A') AS ConceptoRetiro,
		ISNULL(c.Cantidad, 0.00) AS CantidadRetiro,
		COALESCE(CONVERT(VARCHAR(255), c.Fecha), 'N/A') AS FechaRetiro,
		a.Cantidad AS Caja
	FROM 
		Caja AS a
		LEFT OUTER JOIN IngresoEfectivo AS b
		ON a.IDIngresoEfectivo = b.IDIngresoEfectivo
		LEFT OUTER JOIN RetiroEfectivo AS c
		ON c.IDRetiroEfectivo = a.IDRetiroEfectivo
		LEFT OUTER JOIN Cliente AS d
		ON d.IDCliente = b.IDCliente
		LEFT OUTER JOIN TipoPago AS e
		ON e.IDTipoPago = b.IDTipoPago
	ORDER BY a.IDCaja DESC
END
