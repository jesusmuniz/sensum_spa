﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaClases
{
    public class Conexion
    {
        public Tuple<string, string, string, string> lecturaJson()
        {
            string fileName = "info.json";
            string path = Path.Combine(Environment.CurrentDirectory, @"readme\", fileName);
            dynamic data = File.ReadAllText(path);
            dynamic stuff = JObject.Parse(data);

            string server = stuff.server;
            string database = stuff.database;
            string user = stuff.user;
            string pass = stuff.pass;

            return Tuple.Create(server, database, user, pass);
        }
        public List<BibliotecaClases.Clientes> obtenerClientes()
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            List<BibliotecaClases.Clientes> columns = new List<BibliotecaClases.Clientes>();

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_mostrarClientes", con);
           
                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the 4 columns... 

                            var obj = new Clientes();

                            obj.IDCliente = reader["IDCliente"].ToString();
                            obj.Nombre = reader["Nombre"].ToString();
                            obj.ApellidoPaterno = reader["ApellidoPaterno"].ToString();
                            obj.ApellidoMaterno = reader["ApellidoMaterno"].ToString();
                            obj.Telefono = reader["Telefono"].ToString();
                            obj.Celular = reader["Celular"].ToString();
                            obj.CorreoElectronico = reader["CorreoElectronico"].ToString();
                            obj.Activo = Convert.ToBoolean(reader["Activo"]);

                            columns.Add(obj);
                        }
                    }

                    reader.Close();
                }
            }
            return columns;
        }
        public List<BibliotecaClases.Caja> obtenerInfoCaja()
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            List<BibliotecaClases.Caja> columns = new List<BibliotecaClases.Caja>();

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_mostrarCaja", con);

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 

                            var obj = new Caja();
                            decimal cantidadIngreso = 0;
                            decimal cantidadRetiro = 0;
                            decimal caja = 0;

                            obj.IDCaja = reader["IDCaja"].ToString();
                            obj.TipoPago = reader["TipoPago"].ToString();
                            obj.NombreCliente = reader["NombreCliente"].ToString();
                            obj.FechaIngreso = reader["FechaIngreso"].ToString();
                            obj.DescripcionIngreso = reader["DescripcionIngreso"].ToString();
                            cantidadIngreso = Convert.ToDecimal(reader["CantidadIngreso"]);
                            obj.CantidadIngreso = decimal.Round(cantidadIngreso, 2);
                            obj.ConceptoRetiro = reader["ConceptoRetiro"].ToString();
                            cantidadRetiro = Convert.ToDecimal(reader["CantidadRetiro"]);
                            obj.CantidadRetiro = decimal.Round(cantidadRetiro, 2);
                            obj.FechaRetiro = reader["FechaRetiro"].ToString();
                            caja = Convert.ToDecimal(reader["Caja"]);
                            obj.CantidadCaja = decimal.Round(caja, 2);
                            columns.Add(obj);
                        }
                    }

                    reader.Close();
                }
            }
            return columns;
        }
        public List<BibliotecaClases.TipoPago> obtenerInfoTipoPago()
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            List<BibliotecaClases.TipoPago> columns = new List<BibliotecaClases.TipoPago>();

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_mostrarTipoPago", con);

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the 4 columns... 

                            var obj = new TipoPago();

                            obj.IDTipoPago = reader["IDTipoPago"].ToString();
                            obj.Descripcion = reader["Descripcion"].ToString();
                            columns.Add(obj);
                        }
                    }
                    reader.Close();
                }
            }
            return columns;
        }
        public List<string> obtenerSoloClientes()
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            List<string> columns = new List<string>();

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_mostrarClientes", con);

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 

                            string nombreCliente = "";

                            nombreCliente = reader["Nombre"].ToString() + " " + reader["ApellidoPaterno"].ToString() + " " + reader["ApellidoMaterno"].ToString() + "-" + reader["IDCliente"].ToString();
                            
                            columns.Add(nombreCliente);
                        }
                    }

                    reader.Close();
                }
            }

            return columns;
        }
        public void ingresoEfectivo(string TipoPago, int IDCliente, string descripcion, decimal cantidad, decimal precio)
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            string mensajeStoredProcedure = "";

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_guardarIngresoEfectivo", con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@tipoPago", SqlDbType.VarChar).Value = TipoPago;
                cmd.Parameters.Add("@IDCliente", SqlDbType.Int).Value = IDCliente;
                cmd.Parameters.Add("@descripcionIngreso", SqlDbType.VarChar).Value = descripcion;
                cmd.Parameters.Add("@cantidad", SqlDbType.Money).Value = cantidad;
                cmd.Parameters.Add("@precio", SqlDbType.Money).Value = precio;

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 
                            mensajeStoredProcedure = reader["Mensaje"].ToString();
                        }
                    }

                    reader.Close();
                }
            }
        }
        public void RetiroEfectivo(string TipoPago, string concepto, decimal cantidad)
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            string mensajeStoredProcedure = "";

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_guardarRetiroEfectivo", con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@tipoPago", SqlDbType.VarChar).Value = TipoPago;
                cmd.Parameters.Add("@concepto", SqlDbType.VarChar).Value = concepto;
                cmd.Parameters.Add("@cantidad", SqlDbType.Money).Value = cantidad;

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 
                            mensajeStoredProcedure = reader["Mensaje"].ToString();
                        }
                    }

                    reader.Close();
                }
            }
        }
        public void guardarCliente(string nombre, string apellidoPaterno, string apellidoMaterno, string telefono, string celular, string correo)
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            string mensajeStoredProcedure = "";

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_guardarNuevoCliente", con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = nombre;
                cmd.Parameters.Add("@apellidoPaterno", SqlDbType.VarChar).Value = apellidoPaterno;
                cmd.Parameters.Add("@apellidoMaterno", SqlDbType.VarChar).Value = apellidoMaterno;
                cmd.Parameters.Add("@telefomo", SqlDbType.VarChar).Value = telefono;
                cmd.Parameters.Add("@celular", SqlDbType.VarChar).Value = celular;
                cmd.Parameters.Add("@correoElectronico", SqlDbType.VarChar).Value = correo;

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 
                            mensajeStoredProcedure = reader["Mensaje"].ToString();
                        }
                    }

                    reader.Close();
                }
            }
        }
        public List<Servicio> obtenerInfoServicios()
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            List<Servicio> columns = new List<Servicio>();

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_mostrarServicios", con);

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 

                            var obj = new Servicio();
                            decimal precio = 0;

                            obj.IDServicio = reader["IDServicio"].ToString();
                            obj.TipoServicio = reader["TipoServicio"].ToString();
                            obj.Descripcion = reader["Servicio"].ToString();
                            precio = Convert.ToDecimal(reader["Precio"]);
                            obj.Precio = decimal.Round(precio, 2);
                            obj.Fecha = reader["Fecha"].ToString();

                            columns.Add(obj);
                        }
                    }

                    reader.Close();
                }
            }
            return columns;
        }
        public Clientes obtenerNombreCliente(int IDCliente)
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            var obj = new Clientes();

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_obtenerNombreCliente", con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IDCliente", SqlDbType.Int).Value = IDCliente;

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 

                            obj.Nombre = reader["NombreCliente"].ToString();
                          
                        }
                    }

                    reader.Close();
                }
            }
            return obj;
        }
        public List<string> obtenerServiciosString()
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            List<string> columns = new List<string>();

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_mostrarServicios", con);

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 

                            string nombreServicio = "";
                            decimal precio = 0;
                            precio = Convert.ToDecimal(reader["Precio"]);
                            precio = decimal.Round(precio, 2);
                            nombreServicio = reader["Servicio"].ToString() + "-" + precio;

                            columns.Add(nombreServicio);
                        }
                    }

                    reader.Close();
                }
            }

            return columns;
        }
        public void editarPrecioServicio(int IDServicio, decimal nuevoPrecio)
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            string mensajeStoredProcedure = "";

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_modificarPrecioServicio", con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IDServicio", SqlDbType.Int).Value = IDServicio;
                cmd.Parameters.Add("@PrecioNuevo", SqlDbType.Money).Value = nuevoPrecio;

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 
                            mensajeStoredProcedure = reader["Mensaje"].ToString();
                        }
                    }

                    reader.Close();
                }
            }
        }
        public List<ServicioCliente> obtenerInfoServicioCliente(int IDCliente)
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            List<ServicioCliente> columns = new List<ServicioCliente>();

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_mostrarServicioCliente", con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IDCliente", SqlDbType.Int).Value = IDCliente;

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 

                            var obj = new ServicioCliente();
                            decimal saldoPendiente = 0;
                            decimal precio = 0;

                            obj.IDServicioCliente = reader["IDServicioCliente"].ToString();
                            obj.Folio = reader["Folio"].ToString();
                            obj.TipoServicio = reader["TipoServicio"].ToString();
                            obj.Servicio = reader["Servicio"].ToString();
                            precio = Convert.ToDecimal(reader["Precio"]);
                            obj.Precio = decimal.Round(precio, 2);
                            saldoPendiente = Convert.ToDecimal(reader["SaldoPendiente"]);
                            obj.SaldoPendiente = decimal.Round(saldoPendiente, 2);
                            obj.FechaCompra = reader["Fecha"].ToString();
                      
                            columns.Add(obj);
                        }
                    }

                    reader.Close();
                }
            }
            return columns;
        }
        public ServicioCliente obtenerInfoTicket(int IDCaja)
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            var obj = new ServicioCliente();

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_obtenerInfoTicket", con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@IDCaja", SqlDbType.Int).Value = IDCaja;

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 
                            decimal precio = 0;
                            decimal importe = 0;
                            decimal pendiente = 0;

                            obj.Folio = reader["Folio"].ToString();
                            obj.Servicio = reader["Servicio"].ToString();
                            precio = Convert.ToDecimal(reader["Precio"]);
                            obj.Precio = decimal.Round(precio, 2);
                            importe = Convert.ToDecimal(reader["Importe"]);
                            obj.Importe = decimal.Round(importe, 2);
                            pendiente = Convert.ToDecimal(reader["SaldoPendiente"]);
                            obj.SaldoPendiente = decimal.Round(pendiente, 2);
                            obj.NombreCliente = reader["NombreCliente"].ToString();
                            obj.IDPagoServicioCliente = reader["IDPagoServicioCliente"].ToString();
                            obj.FechaCompra = reader["Fecha"].ToString();
                        }
                    }

                    reader.Close();
                }
            }
            return obj;
        }
        public void editarInfoCliente(int IDCliente, string nombre, string apellidoPaterno, string apellidoMaterno, string telefono, string celular, string correo)
        {
            var resultado = lecturaJson();
            string servidor = resultado.Item1;
            string baseDatos = resultado.Item2;
            string usuario = resultado.Item3;
            string contrasena = resultado.Item4;
            string connetionString = null;
            connetionString = "Data Source=" + servidor + ";Initial Catalog=" + baseDatos + ";Trusted_Connection=True;User ID=" + usuario + ";Password=" + contrasena + "";
            string mensajeStoredProcedure = "";

            using (SqlConnection con = new SqlConnection(connetionString))
            {
                var cmd = new SqlCommand("sp_modificarInfoCliente", con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = IDCliente;
                cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = nombre;
                cmd.Parameters.Add("@apellidoPaterno", SqlDbType.VarChar).Value = apellidoPaterno;
                cmd.Parameters.Add("@apellidoMaterno", SqlDbType.VarChar).Value = apellidoMaterno;
                cmd.Parameters.Add("@telefono", SqlDbType.VarChar).Value = telefono;
                cmd.Parameters.Add("@celular", SqlDbType.VarChar).Value = celular;
                cmd.Parameters.Add("@correo", SqlDbType.VarChar).Value = correo;

                con.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {   ///code works up to this point
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {   // breakpoint here shows the columns... 
                            mensajeStoredProcedure = reader["Mensaje"].ToString();
                        }
                    }

                    reader.Close();
                }
            }
        }
    }
    public class Clientes
    {
        public string IDCliente { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string CorreoElectronico { get; set; }
        public bool Activo { get; set; }
        public decimal Saldo { get; set; }
    }
    public class Caja
    {
        public string IDCaja { get; set; }
        public string TipoPago { get; set; }
        public string NombreCliente { get; set; }
        public string FechaIngreso { get; set; }
        public string DescripcionIngreso { get; set; }
        public decimal CantidadIngreso { get; set; }
        public string ConceptoRetiro { get; set; }
        public decimal CantidadRetiro { get; set; }
        public string FechaRetiro { get; set; }
        public decimal CantidadCaja { get; set; }
    }
    public class TipoPago
    {
        public string IDTipoPago { get; set; }
        public string Descripcion { get; set; }
    }
    public class Servicio
    {
        public string IDServicio { get; set; }
        public string TipoServicio { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
        public string Fecha { get; set; }
    }
    public class ServicioCliente
    {
        public string IDServicioCliente { get; set; }
        public string Folio { get; set; }
        public string TipoServicio { get; set; }
        public string Servicio { get; set; }
        public decimal SaldoPendiente { get; set; }
        public string FechaCompra { get; set; }
        public string NombreCliente { get; set; }
        public decimal Precio { get; set; }
        public decimal Importe { get; set; }
        public string IDPagoServicioCliente { get; set; }
    }
}
